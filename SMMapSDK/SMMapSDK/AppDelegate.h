//
//  AppDelegate.h
//  SMMapSDK
//
//  Created by zhuch on 2017/9/7.
//  Copyright © 2017年 zhuch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMMapManager.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, assign) GMMapViewType mapViewType;



@end

