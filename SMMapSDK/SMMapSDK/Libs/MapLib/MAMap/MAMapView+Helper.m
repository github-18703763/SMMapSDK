//
//  MAMapView+Helper.m
//  BusOnline
//
//  Created by HuangKai on 15/12/18.
//  Copyright © 2015年 Goome. All rights reserved.
//

#import "MAMapView+Helper.h"

@implementation MAMapView (Helper)

- (void)zoomIn{
    CGFloat level = self.zoomLevel + 1;
    if (level > self.maxZoomLevel) {
        level = self.maxZoomLevel;
    }
    [self setZoomLevel:level animated:YES];
}

- (void)zoomOut{
    CGFloat level = self.zoomLevel - 1;
    if (level > self.maxZoomLevel) {
        level = self.maxZoomLevel;
    }
    [self setZoomLevel:level animated:YES];
}

@end
