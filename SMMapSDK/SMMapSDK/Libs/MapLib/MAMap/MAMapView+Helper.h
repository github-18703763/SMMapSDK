//
//  MAMapView+Helper.h
//  BusOnline
//
//  Created by HuangKai on 15/12/18.
//  Copyright © 2015年 Goome. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>

@interface MAMapView (Helper)

- (void)zoomIn;

- (void)zoomOut;

@end
