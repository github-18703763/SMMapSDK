//
//  GM_QMapView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <QMapKit/QMapKit.h>
#import "GMMapViewDelegate.h"

@interface GM_QMapView : QMapView <GMMapViewDelegate>

@property (nonatomic, weak) id<GMMapViewDelegate> gm_delegate;

@end
