//
//  GM_MKCircle.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMCircleDelegate.h"

@interface GM_MKCircle : MKCircle <GMCircleDelegate>

@end
