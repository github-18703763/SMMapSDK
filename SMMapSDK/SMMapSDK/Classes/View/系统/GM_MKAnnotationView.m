//
//  GM_MKAnnotationView.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/12.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_MKAnnotationView.h"

@implementation GM_MKAnnotationView

- (void)gm_SetSelected:(BOOL)selected animated:(BOOL)animated
{
    [self setSelected:selected animated:animated];
}

@end
