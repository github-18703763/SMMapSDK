//
//  GM_MKCircleView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface GM_MKCircleView : MKCircleRenderer <GMOverlayViewDelegate>

@end
