//
//  GM_MKPolyline.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMPolylineDelegate.h"

@interface GM_MKPolyline : MKPolyline <GMPolylineDelegate>

@end
