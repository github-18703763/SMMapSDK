//
//  GM_BMKMapView.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/10.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_BMKMapView.h"
#import "GM_BMKAnnotationView.h"
#import "GMAnnotation.h"
#import "GM_BMKCirecleView.h"
#import "GM_BMKPolylineView.h"
#import "GM_BMKPolygonView.h"

@interface GM_BMKMapView () <BMKMapViewDelegate>

@end

@implementation GM_BMKMapView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
    }
    return self;
}

#pragma mark - GMSMapViewDelegate

- (void)gm_ZoomIn
{
    [self zoomIn];
}

- (void)gm_ZoomOut
{
    [self zoomOut];
}

- (void)gm_setMapType:(NSInteger)mapType
{
    if (mapType == 0) {
        [self setMapType:BMKMapTypeStandard];
    }
    else if (mapType == 1)
    {
        [self setMapType:BMKMapTypeSatellite];
    }
}

- (void)gm_AddAnnotation:(id<GMAnnotation>)annotation
{
    [self addAnnotation:(id<BMKAnnotation>)annotation];
}

- (void)gm_RemoveAnnotation:(id<GMAnnotation>)annotation
{
    [self removeAnnotation:(id<BMKAnnotation>)annotation];
}

- (id<GMAnnotationViewDelegate>)gm_dequeueReusableAnnotationViewWithIdentifier:(NSString *)identifier
{
    return (id<GMAnnotationViewDelegate>)[self dequeueReusableAnnotationViewWithIdentifier:identifier];
}

- (void)gm_AddAnnotations:(NSArray *)annotations
{
    [self addAnnotations:annotations];
}

- (void)gm_RemoveAnnotations:(NSArray *)annotations
{
    [self removeAnnotations:annotations];
}

- (void)gm_SelectAnnotation:(id<GMAnnotation>)annotation animated:(BOOL)animated
{
    [self selectAnnotation:(id<BMKAnnotation>)annotation animated:animated];
}

- (void)gm_DeselectAnnotation:(id<GMAnnotation>)annotation animated:(BOOL)animated
{
    [self deselectAnnotation:(id<BMKAnnotation>)annotation animated:animated];
}

- (void)gm_ShowAnnotations:(NSArray *)annotations animated:(BOOL)animated
{
    [self showAnnotations:annotations animated:animated];
}

- (void)gm_SetCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated
{
    [self setCenterCoordinate:coordinate animated:animated];
}

- (void)gm_AddOverlay:(id<GMOverlay>)overlay
{
    [self addOverlay:(id<BMKOverlay>)overlay];
}

- (void)gm_AddOverlays:(NSArray *)overlays;
{
    [self addOverlays:overlays];
}

- (void)gm_RemoveOverlay:(id<GMOverlay>)overlay;
{
    [self removeOverlay:(id<BMKOverlay>)overlay];
}

- (void)gm_RemoveOverlays:(NSArray *)overlays;
{
    [self removeOverlays:overlays];
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay atIndex:(NSUInteger)index;
{
    [self insertOverlay:(id<BMKOverlay>)overlay atIndex:index];
}

- (void)gm_ExchangeOverlayAtIndex:(NSUInteger)index1 withOverlayAtIndex:(NSUInteger)index2;
{
    [self exchangeOverlayAtIndex:index1 withOverlayAtIndex:index2];
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay aboveOverlay:(id<GMOverlay>)sibling;
{
    [self insertOverlay:(id<BMKOverlay>)overlay aboveOverlay:sibling];
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay belowOverlay:(id<GMOverlay>)sibling;
{
    [self insertOverlay:(id<BMKOverlay>)overlay belowOverlay:sibling];
}

- (CLLocationCoordinate2D)gm_ConvertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view
{
    return [self convertPoint:point toCoordinateFromView:view];
}

#pragma mark - BMKMapViewDelegate
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:viewForAnnotation:)]) {
        GM_BMKAnnotationView * annotationView = (GM_BMKAnnotationView *)[self.gm_delegate gm_mapView:self viewForAnnotation:(id<GMAnnotation>)annotation];
        return annotationView;
    }
    return nil;
}

- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didDeselectAnnotationView:)]) {
        [self.gm_delegate gm_mapView:self didDeselectAnnotationView:(id<GMAnnotationViewDelegate>)view];
    }
}

- (void)mapView:(BMKMapView *)mapView didDeselectAnnotationView:(BMKAnnotationView *)view
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didSelectAnnotationView:)]) {
        [self.gm_delegate gm_mapView:self didSelectAnnotationView:(id<GMAnnotationViewDelegate>)view];
    }
}

- (BMKOverlayView *)mapView:(BMKMapView *)mapView viewForOverlay:(id<BMKOverlay>)overlay
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:viewForOverlay:)]) {
        if ([overlay isKindOfClass:[BMKCircle class]]) {
            GM_BMKCirecleView * gm_bmkCircleView = (GM_BMKCirecleView *)[self.gm_delegate gm_mapView:self viewForOverlay:(id<GMOverlay>)overlay];
            return gm_bmkCircleView;
        }
        else if ([overlay isKindOfClass:[BMKPolyline class]])
        {
            GM_BMKPolylineView * gm_bmkPolylineView = (GM_BMKPolylineView *)[self.gm_delegate gm_mapView:self viewForOverlay:(id<GMOverlay>)overlay];
            return gm_bmkPolylineView;
        }
        else if ([overlay isKindOfClass:[BMKPolygon class]])
        {
            GM_BMKPolygonView * gm_bmkPolygonView = (GM_BMKPolygonView *)[self.gm_delegate gm_mapView:self viewForOverlay:(id<GMOverlay>)overlay];
            return gm_bmkPolygonView;
        }
    }
    return nil;
}

- (void)mapView:(BMKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didAddAnnotationViews:)]) {
        [self.gm_delegate gm_mapView:self didAddAnnotationViews:views];
    }
}

- (void)mapView:(BMKMapView *)mapView annotationViewForBubble:(BMKAnnotationView *)view
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:annotationViewForBubble:)]) {
        [self.gm_delegate gm_mapView:self annotationViewForBubble:(id<GMAnnotationViewDelegate>)view];
    }
}

- (void)mapView:(BMKMapView *)mapView didAddOverlayViews:(NSArray *)overlayViews
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didAddOverlayViews:)]) {
        [self.gm_delegate gm_mapView:self didAddOverlayViews:overlayViews];
    }
}

- (void)mapView:(BMKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:regionWillChangeAnimated:)]) {
        [self.gm_delegate gm_MapView:self regionWillChangeAnimated:animated];
    }
}


- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:regionDidChangeAnimated:)]) {
        [self.gm_delegate gm_MapView:self regionDidChangeAnimated:animated];
    }
}


- (void)mapView:(BMKMapView *)mapView mapWillMoveByUser:(BOOL)wasUserAction;
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapWillZoomByUser:)]) {
        [self.gm_delegate gm_MapView:self mapWillMoveByUser:wasUserAction];
    }
}


- (void)mapView:(BMKMapView *)mapView mapDidMoveByUser:(BOOL)wasUserAction;
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapDidMoveByUser:)]) {
        [self.gm_delegate gm_MapView:self mapDidMoveByUser:wasUserAction];
    }
}


- (void)mapView:(BMKMapView *)mapView mapWillZoomByUser:(BOOL)wasUserAction
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapWillZoomByUser:)]) {
        [self.gm_delegate gm_MapView:self mapWillZoomByUser:wasUserAction];
    }
}


- (void)mapView:(BMKMapView *)mapView mapDidZoomByUser:(BOOL)wasUserAction
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapDidZoomByUser:)]) {
        [self.gm_delegate gm_MapView:self mapDidZoomByUser:wasUserAction];
    }
}


- (void)mapView:(BMKMapView *)mapView didSingleTappedAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(gm_MapView:didSingleTappedAtCoordinate:)]) {
        [self gm_MapView:self didSingleTappedAtCoordinate:coordinate];
    }
}

- (void)mapView:(BMKMapView *)mapView didLongPressedAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(gm_MapView:didLongPressedAtCoordinate:)]) {
        [self gm_MapView:self didLongPressedAtCoordinate:coordinate];
    }
}

@end
