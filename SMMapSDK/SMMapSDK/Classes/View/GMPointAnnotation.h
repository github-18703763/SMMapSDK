//
//  GMPointAnnotation.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMAnnotation.h"

@interface GMPointAnnotation : NSObject <GMAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@property (nonatomic, copy) NSString * title;

@property (nonatomic, copy) NSString * subtitle;

@end
