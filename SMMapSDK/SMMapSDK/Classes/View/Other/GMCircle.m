//
//  GMCircle.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMCircle.h"
#import "GM_QCircle.h"
#import "GM_BMKCircle.h"
#import "GM_MACircle.h"
#import "GM_MKCircle.h"

#import "GMMapManager.h"
#import "AppDelegate.h"

@implementation GMCircle

+ (instancetype)circleWithCenterCoordinate:(CLLocationCoordinate2D)coord radius:(CLLocationDistance)radius
{
    GMCircle * circle = [[GMCircle alloc] init];
    circle->_coordinate = coord;
    circle->_radius = radius;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        GM_MKCircle * gm_mkCircle = [GM_MKCircle circleWithCenterCoordinate:coord radius:radius];
        circle.cirDelegate = gm_mkCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        GM_MACircle * gm_maCircle = [GM_MACircle circleWithCenterCoordinate:coord radius:radius];
        circle.cirDelegate = gm_maCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        GM_BMKCircle * gm_bmkCircle = (GM_BMKCircle *)[GM_BMKCircle circleWithCenterCoordinate:coord radius:radius];
        circle.cirDelegate = gm_bmkCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        GM_QCircle * gm_qCircle = (GM_QCircle *)[GM_QCircle circleWithCenterCoordinate:coord radius:radius];
        circle.cirDelegate = gm_qCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    
    return circle;
}

+ (instancetype)circleWithMapRect:(GMMapRect)mapRect
{
    GMCircle * circle = [[GMCircle alloc] init];
    circle->_mapRect = mapRect;
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        MKMapRect rect = MKMapRectMake(mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height);
        GM_MKCircle * gm_mkCircle = [GM_MKCircle circleWithMapRect:rect];
        circle.cirDelegate = gm_mkCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        MAMapRect rect = MAMapRectMake(mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height);
        GM_MACircle * gm_maCircle = [GM_MACircle circleWithMapRect:rect];
        circle.cirDelegate = gm_maCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        BMKMapRect rect = BMKMapRectMake(mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height);
        GM_BMKCircle * gm_bmkCircle = (GM_BMKCircle *)[GM_BMKCircle circleWithMapRect:rect];
        circle.cirDelegate = gm_bmkCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        QMapRect rect = QMapRectMake(mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height);
        GM_QCircle * gm_qCircle = (GM_QCircle *)[GM_QCircle circleWithMapRect:rect];
        circle.cirDelegate = gm_qCircle;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    
    return circle;
}

- (BOOL)setCircleWithCenterCoordinate:(CLLocationCoordinate2D)coord radius:(CLLocationDistance)radius
{
    self->_coordinate = coord;
    self->_radius = radius;
    
    if (self.cirDelegate && [self.cirDelegate respondsToSelector:@selector(gm_SetCircleWithCenterCoordinate:radius:)]) {
        return [self.cirDelegate gm_SetCircleWithCenterCoordinate:coord radius:radius];
    }
    
    return NO;
}

- (BOOL)setCircleWithMapRect:(GMMapRect)mapRect
{
    _mapRect = mapRect;
    
    if (self.cirDelegate && [self.cirDelegate respondsToSelector:@selector(gm_SetCircleWithMapRect:)]) {
        return [self.cirDelegate gm_SetCircleWithMapRect:mapRect];
    }
    
    return NO;
}

- (id<GMCircleDelegate>)circleWithMapType:(GMMapViewType)mapType
{
    if (mapType == GMMapViewTypeSystemMap) {
        GM_MKCircle * gm_mkCircle = [GM_MKCircle circleWithCenterCoordinate:self.coordinate radius:self.radius];
        return gm_mkCircle;
    }
    else if (mapType == GMMapViewTypeMAMap)
    {
        GM_MACircle * gm_maCircle = [GM_MACircle circleWithCenterCoordinate:self.coordinate radius:self.radius];
        return gm_maCircle;
    }
    else if (mapType == GMMapViewTypeBMKMap)
    {
        GM_BMKCircle * gm_bmkCircle = (GM_BMKCircle *)[GM_BMKCircle circleWithCenterCoordinate:self.coordinate radius:self.radius];
        return gm_bmkCircle;
    }
    else if (mapType == GMMapViewTypeQMap)
    {
        GM_QCircle * gm_qCircle = (GM_QCircle *)[GM_QCircle circleWithCenterCoordinate:self.coordinate radius:self.radius];
        return gm_qCircle;
    }
    else if (mapType == GMMapViewTypeGoogleMap)
    {
        
    }
    return nil;
}

@end
