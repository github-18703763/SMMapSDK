//
//  GMPolygon.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMPolygonDelegate.h"
#import "GMMapManager.h"
#import "GMPolygonDelegate.h"

@interface GMPolygon : NSObject

@property (nonatomic, assign) GMMapPoint * points;
@property (nonatomic, assign) NSUInteger count;
@property (nonatomic, assign) CLLocationCoordinate2D * coords;

@property (nonatomic, strong) id<GMPolygonDelegate> polygonDelegate;

/**
 *根据多个点生成多边形
 *@param points 直角坐标点数组，这些点将被拷贝到生成的多边形对象中
 *@param count 点的个数
 *@return 新生成的多边形对象
 */
+ (instancetype)polygonWithPoints:(GMMapPoint *)points count:(NSUInteger)count;

/**
 *根据多个点生成多边形
 *@param coords 经纬度坐标点数组，这些点将被拷贝到生成的多边形对象中
 *@param count 点的个数
 *@return 新生成的多边形对象
 */
+ (instancetype)polygonWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSUInteger)count;

/**
 *重新设置多边形顶点
 *@param points 指定的直角坐标点数组
 *@param count 坐标点的个数
 *@return 是否设置成功
 */
- (BOOL)setPolygonWithPoints:(GMMapPoint *)points count:(NSInteger) count;

/**
 *重新设置多边形顶点
 *@param coords 指定的经纬度坐标点数组
 *@param count 坐标点的个数
 *@return 是否设置成功
 */
- (BOOL)setPolygonWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger) count;

- (id<GMPolygonDelegate>)polylineWithMapType:(GMMapViewType)mapType;

@end
