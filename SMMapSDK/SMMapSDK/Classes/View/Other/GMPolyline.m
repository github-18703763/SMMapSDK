//
//  GMPolyline.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMPolyline.h"
#import "GM_BMKPolyline.h"
#import "GM_MKPolyline.h"
#import "GM_MAPolyline.h"
#import "GM_QPolyline.h"

#import "AppDelegate.h"

@implementation GMPolyline

+ (instancetype)polylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSUInteger)count
{
    GMPolyline * polyline = [[GMPolyline alloc] init];
    polyline.coords = coords;
    polyline.count = count;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        GM_MKPolyline * gm_mkPolyline = [GM_MKPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_mkPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        GM_MAPolyline * gm_maPolyline = [GM_MAPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_maPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        GM_BMKPolyline * gm_bmkPolyline = (GM_BMKPolyline *)[GM_BMKPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_bmkPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        GM_QPolyline * gm_qPolyline = (GM_QPolyline *)[GM_QPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_qPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    
    return polyline;
}

+ (instancetype)polylineWithPoints:(GMMapPoint *)points count:(NSUInteger)count
{
    GMPolyline * polyline = [[GMPolyline alloc] init];
    polyline.points = points;
    polyline.count = count;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        MKMapPoint mkP[count];
        for (int i = 0; i < count; i++) {
            mkP[i] = MKMapPointMake(points[i].x, points[i].y);
        }
        GM_MKPolyline * gm_mkPolyline = [GM_MKPolyline polylineWithPoints:mkP count:count];
        polyline.polylineDelegate = gm_mkPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        MAMapPoint maP[count];
        for (int i = 0; i < count; i++) {
            maP[i] = MAMapPointMake(points[i].x, points[i].y);
        }
        GM_MAPolyline * gm_maPolyline = [GM_MAPolyline polylineWithPoints:maP count:count];
        polyline.polylineDelegate = gm_maPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        BMKMapPoint bmkP[count];
        for (int i = 0; i < count; i++) {
            bmkP[i] = BMKMapPointMake(points[i].x, points[i].y);
        }
        GM_BMKPolyline * gm_bmkPolyline = (GM_BMKPolyline *)[GM_BMKPolyline polylineWithPoints:bmkP count:count];
        polyline.polylineDelegate = gm_bmkPolyline;
        
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        QMapPoint qP[count];
        for (int i = 0; i < count; i++) {
            qP[i] = QMapPointMake(points[i].x, points[i].y);
        }
        GM_QPolyline * gm_qPolyline = (GM_QPolyline *)[GM_QPolyline polylineWithPoints:qP count:count];
        polyline.polylineDelegate = gm_qPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    return polyline;
}

+ (instancetype)polylineWithPoints:(GMMapPoint *)points count:(NSUInteger)count textureIndex:(NSArray *)textureIndex
{
    GMPolyline * polyline = [[GMPolyline alloc] init];
    polyline.points = points;
    polyline.count = count;
    polyline.textureIndex = textureIndex;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        MKMapPoint mkP[count];
        for (int i = 0; i < count; i++) {
            mkP[i] = MKMapPointMake(points[i].x, points[i].y);
        }
        GM_MKPolyline * gm_mkPolyline = [GM_MKPolyline polylineWithPoints:mkP count:count];
        polyline.polylineDelegate = gm_mkPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        MAMapPoint maP[count];
        for (int i = 0; i < count; i++) {
            maP[i] = MAMapPointMake(points[i].x, points[i].y);
        }
        GM_MAPolyline * gm_maPolyline = [GM_MAPolyline polylineWithPoints:maP count:count];
        polyline.polylineDelegate = gm_maPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        BMKMapPoint bmkP[count];
        for (int i = 0; i < count; i++) {
            bmkP[i] = BMKMapPointMake(points[i].x, points[i].y);
        }
        GM_BMKPolyline * gm_bmkPolyline = (GM_BMKPolyline *)[GM_BMKPolyline polylineWithPoints:bmkP count:count textureIndex:textureIndex];
        polyline.polylineDelegate = gm_bmkPolyline;
        
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        QMapPoint qP[count];
        for (int i = 0; i < count; i++) {
            qP[i] = QMapPointMake(points[i].x, points[i].y);
        }
        GM_QPolyline * gm_qPolyline = (GM_QPolyline *)[GM_QPolyline polylineWithPoints:qP count:count];
        polyline.polylineDelegate = gm_qPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    return polyline;
}

+ (instancetype)polylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSUInteger)count textureIndex:(NSArray *)textureIndex
{
    GMPolyline * polyline = [[GMPolyline alloc] init];
    polyline.coords = coords;
    polyline.count = count;
    polyline.textureIndex = textureIndex;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        GM_MKPolyline * gm_mkPolyline = [GM_MKPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_mkPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        GM_MAPolyline * gm_maPolyline = [GM_MAPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_maPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        GM_BMKPolyline * gm_bmkPolyline = (GM_BMKPolyline *)[GM_BMKPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_bmkPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        GM_QPolyline * gm_qPolyline = (GM_QPolyline *)[GM_QPolyline polylineWithCoordinates:coords count:count];
        polyline.polylineDelegate = gm_qPolyline;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    return polyline;
}

- (BOOL)setPolylineWithPoints:(GMMapPoint *)points count:(NSInteger)count
{
    self.points = points;
    self.count = count;
    
    if (self.polylineDelegate && [self.polylineDelegate respondsToSelector:@selector(gm_SetPolylineWithPoints:count:)]) {
        return [self.polylineDelegate gm_SetPolylineWithPoints:points count:count];
    }
    return NO;
}

- (BOOL)setPolylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger)count
{
    self.coords = coords;
    self.count = count;
    
    if (self.polylineDelegate && [self.polylineDelegate respondsToSelector:@selector(gm_SetPolylineWithCoordinates:count:)]) {
        return [self.polylineDelegate gm_SetPolylineWithCoordinates:coords count:count];
    }
    return NO;
}

- (BOOL)setPolylineWithPoints:(GMMapPoint *)points count:(NSInteger)count textureIndex:(NSArray *)textureIndex
{
    self.points = points;
    self.count = count;
    self.textureIndex = textureIndex;
    if (self.polylineDelegate && [self.polylineDelegate respondsToSelector:@selector(gm_SetPolylineWithPoints:count:textureIndex:)]) {
        return [self.polylineDelegate gm_SetPolylineWithPoints:points count:count textureIndex:textureIndex];
    }
    return NO;
}

- (BOOL)setPolylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger)count textureIndex:(NSArray *)textureIndex
{
    self.coords = coords;
    self.count = count;
    self.textureIndex = textureIndex;
    if (self.polylineDelegate && [self.polylineDelegate respondsToSelector:@selector(gm_SetPolylineWithCoordinates:count:textureIndex:)]) {
        return [self.polylineDelegate gm_SetPolylineWithCoordinates:coords count:count textureIndex:textureIndex];
    }
    return NO;
}

- (id<GMPolylineDelegate>)polylineWithMapType:(GMMapViewType)mapType
{
    if (mapType == GMMapViewTypeSystemMap) {
        GM_MKPolyline * gm_mkPolyline = [GM_MKPolyline polylineWithCoordinates:self.coords count:self.count];
        return gm_mkPolyline;
    }
    else if (mapType == GMMapViewTypeMAMap)
    {
        GM_MAPolyline * gm_maPolyline = [GM_MAPolyline polylineWithCoordinates:self.coords count:self.count];
        return gm_maPolyline;
    }
    else if (mapType == GMMapViewTypeBMKMap)
    {
        GM_BMKPolyline * gm_bmkPolyline = (GM_BMKPolyline *)[GM_BMKPolyline polylineWithCoordinates:self.coords count:self.count];
        return gm_bmkPolyline;
    }
    else if (mapType == GMMapViewTypeQMap)
    {
        GM_QPolyline * gm_qPolyline = (GM_QPolyline *)[GM_QPolyline polylineWithCoordinates:self.coords count:self.count];
        return gm_qPolyline;
    }
    else if (mapType == GMMapViewTypeGoogleMap)
    {
    }
    return nil;
}

@end
