//
//  GMPolygon.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMPolygon.h"
#import "GM_BMKPolygon.h"
#import "GM_MAPolygon.h"
#import "GM_MKPolygon.h"
#import "GM_QPolygon.h"

#import "AppDelegate.h"

@implementation GMPolygon

+ (instancetype)polygonWithPoints:(GMMapPoint *)points count:(NSUInteger)count
{
    GMPolygon * polygon = [[GMPolygon alloc] init];
    polygon.points = points;
    polygon.count = count;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        MKMapPoint mkP[count];
        for (int i = 0; i < count; i++) {
            mkP[i] = MKMapPointMake(points[i].x, points[i].y);
        }
        GM_MKPolygon * gm_mkPolygon = [GM_MKPolygon polygonWithPoints:mkP count:count];
        polygon.polygonDelegate = gm_mkPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        MAMapPoint maP[count];
        for (int i = 0; i < count; i++) {
            maP[i] = MAMapPointMake(points[i].x, points[i].y);
        }
        GM_MAPolygon * gm_maPolygon = [GM_MAPolygon polygonWithPoints:maP count:count];
        polygon.polygonDelegate = gm_maPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        BMKMapPoint bmkP[count];
        for (int i = 0; i < count; i++) {
            bmkP[i] = BMKMapPointMake(points[i].x, points[i].y);
        }
        GM_BMKPolygon * gm_bmkPolygon = (GM_BMKPolygon *)[GM_BMKPolygon polygonWithPoints:bmkP count:count];
        polygon.polygonDelegate = gm_bmkPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        QMapPoint qP[count];
        for (int i = 0; i < count; i++) {
            qP[i] = QMapPointMake(points[i].x, points[i].y);
        }
        GM_QPolygon * gm_qPolygon = (GM_QPolygon *)[GM_QPolygon polygonWithPoints:qP count:count];
        polygon.polygonDelegate = gm_qPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    return polygon;
}

+ (instancetype)polygonWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSUInteger)count
{
    GMPolygon * polygon = [[GMPolygon alloc] init];
    polygon.coords = coords;
    polygon.count = count;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.mapViewType == GMMapViewTypeSystemMap) {
        GM_MKPolygon * gm_mkPolygon = [GM_MKPolygon polygonWithCoordinates:coords count:count];
        polygon.polygonDelegate = gm_mkPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeMAMap)
    {
        GM_MAPolygon * gm_maPolygon = [GM_MAPolygon polygonWithCoordinates:coords count:count];
        polygon.polygonDelegate = gm_maPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeBMKMap)
    {
        GM_BMKPolygon * gm_bmkPolygon = (GM_BMKPolygon *)[GM_BMKPolygon polygonWithCoordinates:coords count:count];
        polygon.polygonDelegate = gm_bmkPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeQMap)
    {
        GM_QPolygon * gm_qPolygon = (GM_QPolygon *)[GM_QPolygon polygonWithCoordinates:coords count:count];
        polygon.polygonDelegate = gm_qPolygon;
    }
    else if (appDelegate.mapViewType == GMMapViewTypeGoogleMap)
    {
    }
    return polygon;
}

- (BOOL)setPolygonWithPoints:(GMMapPoint *)points count:(NSInteger)count
{
    _points = points;
    _count = count;
    return YES;
}

- (BOOL)setPolygonWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger)count
{
    _coords = coords;
    _count = count;
    return YES;
}

- (id<GMPolygonDelegate>)polylineWithMapType:(GMMapViewType)mapType
{
    if (mapType == GMMapViewTypeSystemMap) {
        GM_MKPolygon * gm_mkPolygon = [GM_MKPolygon polygonWithCoordinates:self.coords count:self.count];
        return gm_mkPolygon;
    }
    else if (mapType == GMMapViewTypeMAMap)
    {
        GM_MAPolygon * gm_maPolygon = [GM_MAPolygon polygonWithCoordinates:self.coords count:self.count];
        return gm_maPolygon;
    }
    else if (mapType == GMMapViewTypeBMKMap)
    {
        GM_BMKPolygon * gm_bmkPolygon = (GM_BMKPolygon *)[GM_BMKPolygon polygonWithCoordinates:self.coords count:self.count];
        return gm_bmkPolygon;
    }
    else if (mapType == GMMapViewTypeQMap)
    {
        GM_QPolygon * gm_qPolygon = (GM_QPolygon *)[GM_QPolygon polygonWithCoordinates:self.coords count:self.count];
        return gm_qPolygon;
    }
    else if (mapType == GMMapViewTypeGoogleMap)
    {
    }
    return nil;
}

@end
