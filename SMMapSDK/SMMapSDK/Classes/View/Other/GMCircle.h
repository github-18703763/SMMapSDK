//
//  GMCircle.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMCircleDelegate.h"
#import "GMMapManager.h"

@interface GMCircle : NSObject
{
@package
    BOOL _invalidate;
    CLLocationCoordinate2D _coordinate;
    CLLocationDistance _radius;
    GMMapRect _mapRect;
}

/**
 *根据中心点和半径生成圆
 *@param coord 中心点的经纬度坐标
 *@param radius 半径，单位：米
 *@return 新生成的圆
 */
+ (instancetype)circleWithCenterCoordinate:(CLLocationCoordinate2D)coord
                                   radius:(CLLocationDistance)radius;

/**
 *根据指定的直角坐标矩形生成圆，半径由较长的那条边决定，radius = MAX(width, height)/2
 *@param mapRect 指定的直角坐标矩形
 *@return 新生成的圆
 */
+ (instancetype)circleWithMapRect:(GMMapRect)mapRect;

/// 中心点坐标
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

/// 半径，单位：米
@property (nonatomic, assign) CLLocationDistance radius;

/// 该圆的外接矩形
@property (nonatomic, readonly) GMMapRect mapRect;

/// 坐标点数组
@property (nonatomic, readonly) GMMapPoint *points;

/// 坐标点的个数
@property (nonatomic, readonly) NSUInteger pointCount;

/// 要显示的标题
@property (nonatomic, copy) NSString *title;
/// 要显示的副标题
@property (nonatomic, copy) NSString *subtitle;

@property (nonatomic, strong) id<GMCircleDelegate> cirDelegate;

/**
 *设置圆的中心点和半径
 *@param coord 中心点的经纬度坐标
 *@param radius 半径，单位：米
 *@return 是否设置成功
 */
- (BOOL)setCircleWithCenterCoordinate:(CLLocationCoordinate2D)coord radius:(CLLocationDistance)radius;
/**
 *根据指定的直角坐标矩形设置圆，半径由较长的那条边决定，radius = MAX(width, height)/2
 *@param mapRect 指定的直角坐标矩形
 *@return 是否设置成功
 */
- (BOOL)setCircleWithMapRect:(GMMapRect)mapRect;

- (id<GMCircleDelegate>)circleWithMapType:(GMMapViewType)mapType;

@end
