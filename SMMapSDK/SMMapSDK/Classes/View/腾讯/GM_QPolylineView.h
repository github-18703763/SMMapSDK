//
//  GM_QPolylineView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <QMapKit/QMapKit.h>

@interface GM_QPolylineView : QPolylineView <GMOverlayViewDelegate>

@end
