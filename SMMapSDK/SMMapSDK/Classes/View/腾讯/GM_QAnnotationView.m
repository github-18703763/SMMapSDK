//
//  GM_QAnnotationView.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/12.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_QAnnotationView.h"

@implementation GM_QAnnotationView

- (void)gm_SetSelected:(BOOL)selected animated:(BOOL)animated
{
    [self setSelected:selected animated:animated];
}

@end
