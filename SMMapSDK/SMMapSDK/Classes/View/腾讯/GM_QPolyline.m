//
//  GM_QPolyline.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_QPolyline.h"

@implementation GM_QPolyline
@synthesize mapRect = _mapRect;
@end

@implementation QPolyline (Protocol)

@dynamic mapRect;
@end