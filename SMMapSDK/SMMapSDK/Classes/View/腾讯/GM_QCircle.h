//
//  GM_QCircle.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMCircleDelegate.h"

@interface GM_QCircle : QCircle <GMCircleDelegate>

@end

/**
 *  坑爹的腾讯 初始化返回类型固定 即使子类继承初始化也是返回父类类型  导致对象不会带有协议，故增加分类遵守协议
 */
@interface QCircle (Protocol) <GMCircleDelegate>

@end