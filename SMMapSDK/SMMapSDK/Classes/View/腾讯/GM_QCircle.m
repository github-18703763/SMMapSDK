//
//  GM_QCircle.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_QCircle.h"

@implementation GM_QCircle

@synthesize mapRect = _mapRect;
@end

@implementation QCircle (Protocol)

@dynamic mapRect;
@end