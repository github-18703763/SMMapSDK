//
//  GM_QPolygon.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMPolygonDelegate.h"

@interface GM_QPolygon : QPolygon <GMPolygonDelegate>

@end

@interface GM_QPolygon (Protocol) <GMPolygonDelegate>

@end