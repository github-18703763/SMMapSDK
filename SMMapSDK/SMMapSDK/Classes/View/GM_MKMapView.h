//
//  GM_MKMapView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "GMMapViewDelegate.h"

@interface GM_MKMapView : MKMapView <GMMapViewDelegate>

@property (nonatomic, assign) NSInteger zoomLevel;

@property (nonatomic, weak) id<GMMapViewDelegate> gm_delegate;

@end
