//
//  GM_MACircle.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMCircleDelegate.h"

@interface GM_MACircle : MACircle <GMCircleDelegate>

@end
