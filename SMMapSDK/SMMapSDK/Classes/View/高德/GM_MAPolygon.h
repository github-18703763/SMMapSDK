//
//  GM_MAPolygon.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMPolygonDelegate.h"

@interface GM_MAPolygon : MAPolygon <GMPolygonDelegate>

@end

@interface MAPolygon (Protocol) <GMPolygonDelegate>

@end