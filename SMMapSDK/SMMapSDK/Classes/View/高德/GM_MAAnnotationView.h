//
//  GM_MAAnnotationView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/12.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>

@interface GM_MAAnnotationView : MAPinAnnotationView <GMAnnotationViewDelegate>

@end
