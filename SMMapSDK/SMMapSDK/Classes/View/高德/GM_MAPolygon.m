//
//  GM_MAPolygon.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_MAPolygon.h"

@implementation GM_MAPolygon
@synthesize mapRect = _mapRect;
@end

@implementation MAPolygon (Protocol)
@dynamic mapRect;
@end