//
//  GM_MAPolylineView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>

@interface GM_MAPolylineView : MAPolylineRenderer <GMOverlayViewDelegate>

@end
