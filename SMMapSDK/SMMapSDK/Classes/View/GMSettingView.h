//
//  GMSettingView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GMSetViewDelegate <NSObject>

@optional
- (void)changeMapViewIndex:(NSInteger)index;

- (void)mapViewZoomIn;
- (void)mapViewZoonOut;
- (void)mapViewAddAnnotation;
- (void)mapViewRemoveAnnotation;
- (void)mapViewAddOverlay;
- (void)mapViewRemoveOverlay;
- (void)mapViewMoveCenterCoor;
- (void)mapChangeMapModel;

@end

@interface GMSettingView : UIView

@property (nonatomic, assign) NSInteger selectIndex;

@property (nonatomic, weak) id<GMSetViewDelegate> delegate;

@end
