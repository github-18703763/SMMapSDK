//
//  GM_BMKMapView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/10.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMMapViewDelegate.h"

@interface GM_BMKMapView : BMKMapView <GMMapViewDelegate>

@property (nonatomic, weak) id<GMMapViewDelegate> gm_delegate;

@end
