//
//  GM_BMKPolygon.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_BMKPolygon.h"

@implementation GM_BMKPolygon
@synthesize mapRect = _mapRect;

- (BOOL)gm_SetPolygonWithPoints:(GMMapPoint *)points count:(NSInteger)count
{
    BMKMapPoint bmkP[count];
    for (int i = 0; i < count; i++) {
        bmkP[i] = BMKMapPointMake(points[i].x, points[i].y);
    }
    return [self setPolygonWithPoints:bmkP count:count];
}

- (BOOL)gm_SetPolygonWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger)count
{
    return [self setPolygonWithCoordinates:coords count:count];
}

@end

@implementation BMKPolygon (Procotol)

@end