//
//  GM_BMKAnnotationView.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_BMKAnnotationView.h"

@implementation GM_BMKAnnotationView

- (void)gm_SetSelected:(BOOL)selected animated:(BOOL)animated
{
    [self setSelected:selected animated:animated];
}

@end
