//
//  GM_BMKAnnotationView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMAnnotationViewDelegate.h"

@interface GM_BMKAnnotationView : BMKPinAnnotationView <GMAnnotationViewDelegate>

@end
