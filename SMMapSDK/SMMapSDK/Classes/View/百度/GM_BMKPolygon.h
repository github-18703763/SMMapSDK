//
//  GM_BMKPolygon.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMPolygonDelegate.h"

@interface GM_BMKPolygon : BMKPolygon <GMPolygonDelegate>

@end

@interface BMKPolygon (Protocol) <GMPolygonDelegate>

@end