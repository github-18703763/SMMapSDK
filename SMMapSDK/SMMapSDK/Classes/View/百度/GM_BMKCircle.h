//
//  GM_BMKCircle.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMCircleDelegate.h"

@interface GM_BMKCircle : BMKCircle <GMCircleDelegate>

+ (instancetype)circleWithCenterCoordinate:(CLLocationCoordinate2D)coord
                                   radius:(CLLocationDistance)radius;

/**
 *根据指定的直角坐标矩形生成圆，半径由较长的那条边决定，radius = MAX(width, height)/2
 *@param mapRect 指定的直角坐标矩形
 *@return 新生成的圆
 */
+ (instancetype)circleWithMapRect:(BMKMapRect)mapRect;

@end


/**
 *  坑爹的百度 初始化返回类型固定 即使子类继承初始化也是返回父类类型  导致对象不会带有协议，故增加分类遵守协议
 */
@interface BMKCircle (Protocol) <GMCircleDelegate>

@end
