//
//  GM_BMKPolyline.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//
#import "GMPolylineDelegate.h"

@interface GM_BMKPolyline : BMKPolyline <GMPolylineDelegate>

@end

@interface BMKPolyline (Protocol) <GMPolylineDelegate>

@end