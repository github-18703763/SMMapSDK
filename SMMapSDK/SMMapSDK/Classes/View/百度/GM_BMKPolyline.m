//
//  GM_BMKPolyline.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_BMKPolyline.h"

@implementation GM_BMKPolyline
@synthesize mapRect = _mapRect;

- (BOOL)gm_SetPolylineWithPoints:(GMMapPoint *)points count:(NSInteger) count
{
    BMKMapPoint bmkP[count];
    for (int i = 0; i < count; i++) {
        bmkP[i] = BMKMapPointMake(points[i].x, points[i].y);
    }
    return [self setPolylineWithPoints:bmkP count:count];
}

- (BOOL)gm_SetPolylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger) count
{
    return [self setPolylineWithCoordinates:coords count:count];
}

- (BOOL)gm_SetPolylineWithPoints:(GMMapPoint *)points count:(NSInteger) count textureIndex:(NSArray*) textureIndex;
{
    BMKMapPoint bmkP[count];
    for (int i = 0; i < count; i++) {
        bmkP[i] = BMKMapPointMake(points[i].x, points[i].y);
    }
    return [self setPolylineWithPoints:bmkP count:count textureIndex:textureIndex];
}

- (BOOL)gm_SetPolylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger) count textureIndex:(NSArray*) textureIndex;
{
    return [self setPolylineWithCoordinates:coords count:count textureIndex:textureIndex];
}

@end

@implementation BMKPolyline (Protocol)
@dynamic mapRect;
@end