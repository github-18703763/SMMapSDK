//
//  GM_BMKCircle.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_BMKCircle.h"

@implementation GM_BMKCircle

@synthesize mapRect = _mapRect;

+ (instancetype)circleWithCenterCoordinate:(CLLocationCoordinate2D)coord radius:(CLLocationDistance)radius
{
    GM_BMKCircle * circle = (GM_BMKCircle *)[super circleWithCenterCoordinate:coord radius:radius];
    return circle;
}

+ (instancetype)circleWithMapRect:(BMKMapRect)mapRect
{
    GM_BMKCircle * circle = (GM_BMKCircle *)[super circleWithMapRect:mapRect];
    return circle;
}

- (BOOL)gm_SetCircleWithCenterCoordinate:(CLLocationCoordinate2D)coord radius:(CLLocationDistance)radius
{
    return [self setCircleWithCenterCoordinate:coord radius:radius];
}

- (BOOL)gm_SetCircleWithMapRect:(GMMapRect)mapRect
{
    BMKMapRect rect = BMKMapRectMake(mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height);
    return [self setCircleWithMapRect:rect];
}

@end

@implementation BMKCircle (Protocol)

@dynamic mapRect;
@end