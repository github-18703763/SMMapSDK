//
//  GM_MAMapView.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GM_MAMapView.h"
#import "GM_MAAnnotationView.h"
#import "GM_MACircleView.h"
#import "GM_MAPolylineView.h"
#import "GM_MAPolygonView.h"

@interface GM_MAMapView () <MAMapViewDelegate>

@end

@implementation GM_MAMapView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
    }
    return self;
}

#pragma mark - GMMapViewDelegate

- (void)gm_ZoomIn
{
    CGFloat level = self.zoomLevel + 1;
    if (level > self.maxZoomLevel) {
        level = self.maxZoomLevel;
    }
    [self setZoomLevel:level animated:YES];
}

- (void)gm_ZoomOut
{
    CGFloat level = self.zoomLevel - 1;
    if (level < self.minZoomLevel) {
        level = self.minZoomLevel;
    }
    [self setZoomLevel:level animated:YES];
}

- (void)gm_setMapType:(NSInteger)mapType
{
    if (mapType == 0) {
        [self setMapType:MAMapTypeStandard];
    }
    else if (mapType == 1)
    {
        [self setMapType:MAMapTypeSatellite];
    }
}

- (void)gm_AddAnnotation:(id<GMAnnotation>)annotation
{
    [self addAnnotation:(id<MAAnnotation>)annotation];
}

- (void)gm_RemoveAnnotation:(id<GMAnnotation>)annotation
{
    [self removeAnnotation:(id<MAAnnotation>)annotation];
}

- (id<GMAnnotationViewDelegate>)gm_dequeueReusableAnnotationViewWithIdentifier:(NSString *)identifier
{
    return (id<GMAnnotationViewDelegate>)[self dequeueReusableAnnotationViewWithIdentifier:identifier];
}

- (void)gm_AddAnnotations:(NSArray *)annotations
{
    [self addAnnotations:annotations];
}

- (void)gm_RemoveAnnotations:(NSArray *)annotations
{
    [self removeAnnotations:annotations];
}

- (void)gm_SelectAnnotation:(id<GMAnnotation>)annotation animated:(BOOL)animated
{
    [self selectAnnotation:(id<MAAnnotation>)annotation animated:animated];
}

- (void)gm_DeselectAnnotation:(id<GMAnnotation>)annotation animated:(BOOL)animated
{
    [self deselectAnnotation:(id<MAAnnotation>)annotation animated:animated];
}

- (void)gm_ShowAnnotations:(NSArray *)annotations animated:(BOOL)animated
{
    [self showAnnotations:annotations animated:animated];
}

- (void)gm_SetCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated
{
    [self setCenterCoordinate:coordinate animated:animated];
}

- (void)gm_AddOverlay:(id<GMOverlay>)overlay
{
    [self addOverlay:(id<MAOverlay>)overlay];
}

- (void)gm_AddOverlays:(NSArray *)overlays;
{
    [self addOverlays:overlays];
}

- (void)gm_RemoveOverlay:(id<GMOverlay>)overlay;
{
    [self removeOverlay:(id<MAOverlay>)overlay];
}

- (void)gm_RemoveOverlays:(NSArray *)overlays;
{
    [self removeOverlays:overlays];
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay atIndex:(NSUInteger)index;
{
    [self insertOverlay:(id<MAOverlay>)overlay atIndex:index];
}

- (void)gm_ExchangeOverlayAtIndex:(NSUInteger)index1 withOverlayAtIndex:(NSUInteger)index2;
{
    [self exchangeOverlayAtIndex:index1 withOverlayAtIndex:index2];
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay aboveOverlay:(id<GMOverlay>)sibling;
{
    [self insertOverlay:(id<MAOverlay>)overlay aboveOverlay:sibling];
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay belowOverlay:(id<GMOverlay>)sibling;
{
    [self insertOverlay:(id<MAOverlay>)overlay belowOverlay:sibling];
}

- (CLLocationCoordinate2D)gm_ConvertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view
{
    return [self convertPoint:point toCoordinateFromView:view];
}

#pragma mark - MAMapViewDelegate

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:viewForAnnotation:)]) {
        GM_MAAnnotationView * annotationView = (GM_MAAnnotationView *)[self.gm_delegate gm_mapView:self viewForAnnotation:(id<GMAnnotation>)annotation];
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MAMapView *)mapView didDeselectAnnotationView:(MAAnnotationView *)view
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didDeselectAnnotationView:)]) {
        [self.gm_delegate gm_mapView:self didDeselectAnnotationView:(id<GMAnnotationViewDelegate>)view];
    }
}

- (void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didSelectAnnotationView:)]) {
        [self.gm_delegate gm_mapView:self didSelectAnnotationView:(id<GMAnnotationViewDelegate>)view];
    }
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id<MAOverlay>)overlay
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:viewForOverlay:)]) {
        if ([overlay isKindOfClass:[MACircle class]]) {
            GM_MACircleView * gm_maCircleView = (GM_MACircleView *)[self.gm_delegate gm_mapView:self viewForOverlay:(id<GMOverlay>)overlay];
            return gm_maCircleView;
        }
        else if ([overlay isKindOfClass:[MAPolyline class]])
        {
            GM_MAPolylineView * gm_mkPolylineView = (GM_MAPolylineView *)[self.gm_delegate gm_mapView:self viewForOverlay:(id<GMOverlay>)overlay];
            return gm_mkPolylineView;
        }
        else if ([overlay isKindOfClass:[MAPolygon class]])
        {
            GM_MAPolygonView * gm_mkPolygonView = (GM_MAPolygonView *)[self.gm_delegate gm_mapView:self viewForOverlay:(id<GMOverlay>)overlay];
            return gm_mkPolygonView;
        }
    }
    return nil;
}

- (void)mapView:(MAMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didAddAnnotationViews:)]) {
        [self.gm_delegate gm_mapView:self didAddAnnotationViews:views];
    }
}

- (void)mapView:(MAMapView *)mapView annotationViewForBubble:(MAAnnotationView *)view
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:annotationViewForBubble:)]) {
        [self.gm_delegate gm_mapView:self annotationViewForBubble:(id<GMAnnotationViewDelegate>)view];
    }
}

- (void)mapView:(MAMapView *)mapView didAddOverlayViews:(NSArray *)overlayViews
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_mapView:didAddOverlayViews:)]) {
        [self.gm_delegate gm_mapView:self didAddOverlayViews:overlayViews];
    }
}

- (void)mapView:(id<GMMapViewDelegate>)mapView regionWillChangeAnimated:(BOOL)animated
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:regionWillChangeAnimated:)]) {
        [self.gm_delegate gm_MapView:self regionWillChangeAnimated:animated];
    }
}


- (void)mapView:(id<GMMapViewDelegate>)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:regionDidChangeAnimated:)]) {
        [self.gm_delegate gm_MapView:self regionDidChangeAnimated:animated];
    }
}


- (void)mapView:(id<GMMapViewDelegate>)mapView mapWillMoveByUser:(BOOL)wasUserAction;
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapWillZoomByUser:)]) {
        [self.gm_delegate gm_MapView:self mapWillMoveByUser:wasUserAction];
    }
}


- (void)mapView:(id<GMMapViewDelegate>)mapView mapDidMoveByUser:(BOOL)wasUserAction;
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapDidMoveByUser:)]) {
        [self.gm_delegate gm_MapView:self mapDidMoveByUser:wasUserAction];
    }
}


- (void)mapView:(id<GMMapViewDelegate>)mapView mapWillZoomByUser:(BOOL)wasUserAction
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapWillZoomByUser:)]) {
        [self.gm_delegate gm_MapView:self mapWillZoomByUser:wasUserAction];
    }
}


- (void)mapView:(id<GMMapViewDelegate>)mapView mapDidZoomByUser:(BOOL)wasUserAction
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:mapDidZoomByUser:)]) {
        [self.gm_delegate gm_MapView:self mapDidZoomByUser:wasUserAction];
    }
}


- (void)mapView:(id<GMMapViewDelegate>)mapView didSingleTappedAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:didSingleTappedAtCoordinate:)]) {
        [self.gm_delegate gm_MapView:self didSingleTappedAtCoordinate:coordinate];
    }
}

- (void)mapView:(id<GMMapViewDelegate>)mapView didLongPressedAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (self.gm_delegate && [self.gm_delegate respondsToSelector:@selector(gm_MapView:didLongPressedAtCoordinate:)]) {
        [self.gm_delegate gm_MapView:self didLongPressedAtCoordinate:coordinate];
    }
}

@end
