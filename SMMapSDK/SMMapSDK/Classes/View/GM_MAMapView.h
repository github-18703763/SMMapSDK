//
//  GM_MAMapView.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "GMMapViewDelegate.h"

@interface GM_MAMapView : MAMapView <GMMapViewDelegate>

@property (nonatomic, weak) id<GMMapViewDelegate> gm_delegate;

@end
