//
//  GMSettingView.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMSettingView.h"

@interface GMSettingView () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) UIButton * zoomInBtn;
@property (nonatomic, strong) UIButton * zoomOutBtn;
@property (nonatomic, strong) UIButton * addAnnoBtn;
@property (nonatomic, strong) UIButton * removeAnnoBtn;
@property (nonatomic, strong) UIButton * addOverlayBtn;
@property (nonatomic, strong) UIButton * removeOverlayBtn;
@property (nonatomic, strong) UIButton * setCenterBtn;
@property (nonatomic, strong) UIButton * changeMapModelBtn;
@property (nonatomic, strong) NSArray * dateArray;

@end

@implementation GMSettingView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _dateArray = @[@"系统地图", @"高德地图", @"百度地图", @"腾讯地图", @"谷歌地图"];
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    CGFloat tableW = 2 * SCREEN_WIDTH / 5;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, tableW, self.frame.size.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self addSubview:_tableView];
    
    CGFloat originX = _tableView.frame.size.width;
    CGFloat btnW = (SCREEN_WIDTH - tableW) / 2;
    _zoomInBtn = [[UIButton alloc] initWithFrame:CGRectMake(originX, 0, btnW, 30)];
    [_zoomInBtn setTitle:@"放大" forState:UIControlStateNormal];
    [_zoomInBtn addTarget:self action:@selector(scaleIn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_zoomInBtn];
    
    _zoomOutBtn = [[UIButton alloc] initWithFrame:CGRectMake(btnW + tableW, 0, btnW, 30)];
    [_zoomOutBtn setTitle:@"缩小" forState:UIControlStateNormal];
    [_zoomOutBtn addTarget:self action:@selector(scaleOut) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_zoomOutBtn];
    
    _addAnnoBtn = [[UIButton alloc] initWithFrame:CGRectMake(originX, 30, btnW, 30)];
    [_addAnnoBtn setTitle:@"添加标注" forState:UIControlStateNormal];
    [_addAnnoBtn addTarget:self action:@selector(addAnnotation) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_addAnnoBtn];
    
    _removeAnnoBtn = [[UIButton alloc] initWithFrame:CGRectMake(btnW + tableW, 30, btnW, 30)];
    [_removeAnnoBtn setTitle:@"删除标注" forState:UIControlStateNormal];
    [_removeAnnoBtn addTarget:self action:@selector(removeAnnotation) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_removeAnnoBtn];
    
    _addOverlayBtn = [[UIButton alloc] initWithFrame:CGRectMake(originX, 60, btnW, 30)];
    [_addOverlayBtn setTitle:@"添加覆盖物" forState:UIControlStateNormal];
    [_addOverlayBtn addTarget:self action:@selector(addOverlay) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_addOverlayBtn];
    
    _removeOverlayBtn = [[UIButton alloc] initWithFrame:CGRectMake(btnW + tableW, 60, btnW, 30)];
    [_removeOverlayBtn setTitle:@"删除覆盖物" forState:UIControlStateNormal];
    [_removeOverlayBtn addTarget:self action:@selector(removeOverlay) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_removeOverlayBtn];
    
    _setCenterBtn = [[UIButton alloc] initWithFrame:CGRectMake(originX, 90, btnW, 30)];
    [_setCenterBtn setTitle:@"移动中心点" forState:UIControlStateNormal];
    [_setCenterBtn addTarget:self action:@selector(setCenterCoor) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_setCenterBtn];
    
    _changeMapModelBtn = [[UIButton alloc] initWithFrame:CGRectMake(btnW + tableW, 90, btnW, 30)];
    [_changeMapModelBtn setTitle:@"切换地图模式" forState:UIControlStateNormal];
    [_changeMapModelBtn addTarget:self action:@selector(changeMapModel) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_changeMapModelBtn];
    
}

- (void)scaleIn
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewZoomIn)]) {
        [self.delegate mapViewZoomIn];
    }
}

- (void)scaleOut
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewZoonOut)]) {
        [self.delegate mapViewZoonOut];
    }
}

- (void)addAnnotation
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewAddAnnotation)]) {
        [self.delegate mapViewAddAnnotation];
    }
}

- (void)removeAnnotation
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewRemoveAnnotation)]) {
        [self.delegate mapViewRemoveAnnotation];
    }
}

- (void)addOverlay
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewAddOverlay)]) {
        [self.delegate mapViewAddOverlay];
    }
}

- (void)removeOverlay
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewRemoveOverlay)]) {
        [self.delegate mapViewRemoveOverlay];
    }
}

- (void)setCenterCoor
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewMoveCenterCoor)]) {
        [self.delegate mapViewMoveCenterCoor];
    }
}

- (void)changeMapModel
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapChangeMapModel)]) {
        [self.delegate mapChangeMapModel];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellId = @"cellId";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = _dateArray[indexPath.row];
    
    if (_selectIndex == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 4) {
//        UIAlertView * al = [[UIAlertView alloc] initWithTitle:@"提示" message:@"国内无法使用谷歌地图，暂不支持" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [al show];
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeMapViewIndex:)]) {
        [self.delegate changeMapViewIndex:indexPath.row];
    }
    _selectIndex = indexPath.row;
    [_tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    [_tableView reloadData];
}

@end
