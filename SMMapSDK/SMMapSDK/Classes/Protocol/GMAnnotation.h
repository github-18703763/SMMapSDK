//
//  GMAnnotation.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GMAnnotation
<
#ifdef __SMap__
MKAnnotation,
#endif
#ifdef __BMK__
BMKAnnotation,
#endif
#ifdef __AMap__
MAAnnotation,
#endif
#ifdef __QMap__
QAnnotation,
#endif
#ifdef __GoogleMap__
#else
NSObject
#endif
>

///标注view中心坐标.
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@optional

/**
 *获取annotation标题
 *@return 返回annotation的标题信息
 */
- (NSString *)title;

/**
 *获取annotation副标题
 *@return 返回annotation的副标题信息
 */
- (NSString *)subtitle;

/**
 *设置标注的坐标，在拖拽时会被调用.
 *@param newCoordinate 新的坐标值
 */
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@end
