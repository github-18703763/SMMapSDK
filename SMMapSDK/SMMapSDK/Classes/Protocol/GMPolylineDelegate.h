//
//  GMPolylineDelegate.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GMPolylineDelegate <GMOverlay>

@optional
- (BOOL)gm_SetPolylineWithPoints:(GMMapPoint *)points count:(NSInteger) count;
- (BOOL)gm_SetPolylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger) count;

- (BOOL)gm_SetPolylineWithPoints:(GMMapPoint *)points count:(NSInteger) count textureIndex:(NSArray*) textureIndex;
- (BOOL)gm_SetPolylineWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger) count textureIndex:(NSArray*) textureIndex;

@end
