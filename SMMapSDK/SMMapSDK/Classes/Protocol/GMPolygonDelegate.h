//
//  GMPolygonDelegate.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GMPolygonDelegate <GMOverlay>

@optional
- (BOOL)gm_SetPolygonWithPoints:(GMMapPoint *)points count:(NSInteger) count;

- (BOOL)gm_SetPolygonWithCoordinates:(CLLocationCoordinate2D *)coords count:(NSInteger) count;

@end
