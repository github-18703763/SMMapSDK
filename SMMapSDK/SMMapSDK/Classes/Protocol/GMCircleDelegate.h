//
//  GMCircleDelegate.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GMCircleDelegate <GMOverlay>

@optional
- (BOOL)gm_SetCircleWithCenterCoordinate:(CLLocationCoordinate2D)coord radius:(CLLocationDistance)radius;

- (BOOL)gm_SetCircleWithMapRect:(GMMapRect)mapRect;

@end
