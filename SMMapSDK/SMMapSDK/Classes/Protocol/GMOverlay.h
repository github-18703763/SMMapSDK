//
//  GMOverlay.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/12.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>

///地理坐标点，用直角地理坐标表示
typedef struct {
    double x;	///< 横坐标
    double y;	///< 纵坐标
} GMMapPoint;

///矩形大小，用直角地理坐标表示
typedef struct {
    double width;	///< 宽度
    double height;	///< 高度
} GMMapSize;

///矩形，用直角地理坐标表示
typedef struct {
    GMMapPoint origin; ///< 屏幕左上点对应的直角地理坐标
    GMMapSize size;	///< 坐标范围
} GMMapRect;

@protocol GMOverlay
<
#ifdef __SMap__
MKOverlay,
#endif
#ifdef __BMK__
BMKOverlay,
#endif
#ifdef __AMap__
MAOverlay,
#endif
#ifdef __QMap__
QOverlay,
#endif
#ifdef __GoogleMap__
#else
NSObject
#endif
>

@required
/// 返回区域中心坐标.
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

/// 返回区域外接矩形
@property (nonatomic, readonly) GMMapRect mapRect;

@end
