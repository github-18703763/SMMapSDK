//
//  GMAnnotationViewDelegate.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/11.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GMAnnotationViewDelegate <NSObject>

- (void)gm_SetSelected:(BOOL)selected animated:(BOOL)animated;

@end
