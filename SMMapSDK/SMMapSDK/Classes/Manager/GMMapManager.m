//
//  GMMapManager.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/9.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMMapManager.h"
#import "AppDelegate.h"

@implementation GMMapManager

//+ (instancetype)shareManager
//{
//    static GMMapManager * manager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        manager = [[GMMapManager alloc] init];
//        manager.centerCoordinate = kCLLocationCoordinate2DInvalid;
//    });
//    return manager;
//}

- (NSMutableArray<id<GMAnnotation>> *)annoArray
{
    if (!_annoArray) {
        _annoArray = [NSMutableArray array];
    }
    return _annoArray;
}

- (NSMutableArray<id<GMOverlay>> *)overlayArray
{
    if (!_overlayArray) {
        _overlayArray = [NSMutableArray array];
    }
    return _overlayArray;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _centerCoordinate = kCLLocationCoordinate2DInvalid;
    }
    return self;
}

+ (void)registerAMapKey
{
#ifdef __AMap__
    [MAMapServices sharedServices].apiKey = kMAMapKey;
    [AMapSearchServices sharedServices].apiKey = kMAMapKey;
#endif
}

+ (void)registerQMapKey
{
#ifdef __QMap__
    [QMapServices sharedServices].apiKey = kTencentMapKey;
#endif
}

+ (void)registerBMKMapKey
{
#ifdef __BMK__
    BMKMapManager *mapManager = [[BMKMapManager alloc] init];
    [mapManager start:kBMKMapKey generalDelegate:nil] ? NSLog(@"BMKMap register success") : NSLog(@"BMKMap register failure");
#endif
}

+ (void)registerGMSMapKey
{
#ifdef __GoogleMap__
    [GMSServices provideAPIKey:kGoogleMapKey] ? NSLog(@"GoogleMap register success") : NSLog(@"GoogleMap register failure");
#endif
}

/**
 *  切换地图操作
 *
 *  @param mapViewType 地图类型
 */
- (void)setMapViewType:(GMMapViewType)mapViewType
{
    if (_mapViewType == mapViewType) return;
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.mapViewType = mapViewType;
    _mapViewType = mapViewType;
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewDidChange:)]) {
        [self.delegate mapViewDidChange:mapViewType];
    }
    UIView * supView;
    if (self.delegate && [self.delegate respondsToSelector:@selector(superViewForMapView)]) {
       supView  = [self.delegate superViewForMapView];
    }
    switch (mapViewType) {
        case GMMapViewTypeSystemMap:
        {
#ifdef __SMap__
            [(UIView *)self.mapView removeFromSuperview];
            GM_MKMapView * gm_mkMapView = [[GM_MKMapView alloc] init];
            gm_mkMapView.frame = supView.bounds;
            gm_mkMapView.centerCoordinate = CLLocationCoordinate2DMake(39.92, 116.46);
            gm_mkMapView.zoomLevel = 9;
            [supView addSubview:gm_mkMapView];
            [supView sendSubviewToBack:gm_mkMapView];
            gm_mkMapView.gm_delegate = [self.delegate anyDelegate];
            if (self.mapType == GMMapTypeStandard) {
                gm_mkMapView.mapType = MKMapTypeStandard;
            }
            else
            {
                gm_mkMapView.mapType = MKMapTypeSatellite;
            }
            self.mapView = gm_mkMapView;
#endif
        }
            break;
        
        case GMMapViewTypeMAMap:
        {
#ifdef __AMap__
            [(UIView *)self.mapView removeFromSuperview];
            GM_MAMapView * gm_maMapView = [[GM_MAMapView alloc] init];
            gm_maMapView.frame = supView.bounds;
            if (CLLocationCoordinate2DIsValid(self.centerCoordinate)) gm_maMapView.centerCoordinate = self.centerCoordinate;
            [supView addSubview:gm_maMapView];
            [supView sendSubviewToBack:gm_maMapView];
            gm_maMapView.gm_delegate = [self.delegate anyDelegate];
            if (self.mapType == GMMapTypeStandard) {
                gm_maMapView.mapType = MAMapTypeStandard;
            }
            else
            {
                gm_maMapView.mapType = MAMapTypeSatellite;
            }
            self.mapView = gm_maMapView;
#endif
        }
            break;
            
        case GMMapViewTypeBMKMap:
        {
#ifdef __BMK__
            [(UIView *)self.mapView removeFromSuperview];
            GM_BMKMapView * gm_bmkMapView = [[GM_BMKMapView alloc] init];
            gm_bmkMapView.frame = supView.bounds;
            if (CLLocationCoordinate2DIsValid(self.centerCoordinate)) gm_bmkMapView.centerCoordinate = self.centerCoordinate;
            [supView addSubview:gm_bmkMapView];
            [supView sendSubviewToBack:gm_bmkMapView];
            gm_bmkMapView.gm_delegate = [self.delegate anyDelegate];
            if (self.mapType == GMMapTypeStandard) {
                gm_bmkMapView.mapType = BMKMapTypeStandard;
            }
            else
            {
                gm_bmkMapView.mapType = BMKMapTypeSatellite;
            }
            self.mapView = gm_bmkMapView;
#endif
        }
            
            break;
        case GMMapViewTypeQMap:
        {
#ifdef __QMap__
            [(UIView *)self.mapView removeFromSuperview];
            GM_QMapView * gm_qMapView = [[GM_QMapView alloc] initWithFrame:supView.bounds];
            if (CLLocationCoordinate2DIsValid(self.centerCoordinate)) gm_qMapView.centerCoordinate = self.centerCoordinate;
            [supView addSubview:gm_qMapView];
            [supView sendSubviewToBack:gm_qMapView];
            gm_qMapView.gm_delegate = [self.delegate anyDelegate];
            if (self.mapType == GMMapTypeStandard) {
                gm_qMapView.mapType = QMapTypeStandard;
            }
            else
            {
                gm_qMapView.mapType = QMapTypeSatellite;
            }
            self.mapView = gm_qMapView;
#endif
        }
            break;
            
        case GMMapViewTypeGoogleMap:
        {
#ifdef __GoogleMap__
            [(UIView *)self.mapView removeFromSuperview];
            GMSCameraPosition *camera = nil;
            if (CLLocationCoordinate2DIsValid(self.centerCoordinate)) {
                camera = [GMSCameraPosition cameraWithTarget:self.centerCoordinate zoom:9];
            } else {
                camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake(22.6345, 113.456) zoom:9];
            }
            GM_GMSMapView * gm_gmsMapView = [[GM_GMSMapView alloc] init];
            gm_gmsMapView.camera = camera;
            gm_gmsMapView.frame = supView.bounds;
            [gm_gmsMapView setMinZoom:2 maxZoom:18];
            [supView addSubview:gm_gmsMapView];
            [supView sendSubviewToBack:gm_gmsMapView];
            if (self.mapType == GMMapTypeStandard) {
                gm_gmsMapView.mapType = GMMapTypeStandard;
            }
            else
            {
                gm_gmsMapView.mapType = GMMapTypeSatellite;
            }
            self.mapView = gm_gmsMapView;
#endif
        }
            break;
        
        case GMMapViewTypeNone:
            break;
            
        default:
            break;
    }
    
    if (self.annoArray.count > 0) {
        [self.mapView gm_AddAnnotations:self.annoArray];
    }
    
    if (self.overlayArray.count > 0) {
        [self.mapView gm_AddOverlays:self.overlayArray];
    }
}

- (void)setMapType:(GMMapType)mapType
{
    if (_mapType == mapType) {
        return;
    }
    _mapType = mapType;
    switch (mapType) {
        case GMMapTypeStandard:
            [self.mapView gm_setMapType:0];
            break;
            
        case GMMapTypeSatellite:
            [self.mapView gm_setMapType:1];
            break;
        default:
            break;
    }
}

- (void)zoomIn
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_ZoomIn)]) {
        [self.mapView gm_ZoomIn];
    }
}

- (void)zoomOut
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_ZoomOut)]) {
        [self.mapView gm_ZoomOut];
    }
}

- (void)gm_AddAnnotation:(id<GMAnnotation>)annotation
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_AddAnnotation:)]) {
        [self.mapView gm_AddAnnotation:annotation];
    }
    [self.annoArray addObject:annotation];
}

- (void)gm_AddAnnotations:(NSArray *)annotations
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_AddAnnotations:)]) {
        [self.mapView gm_AddAnnotations:annotations];
    }
    [self.annoArray addObjectsFromArray:annotations];
}

- (void)gm_RemoveAnnotation:(id<GMAnnotation>)annotation
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_RemoveAnnotation:)]) {
        [self.mapView gm_RemoveAnnotation:annotation];
    }
    
    [self.annoArray removeObject:annotation];
}

- (void)gm_RemoveAnnotations:(NSArray *)annotations
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_RemoveAnnotations:)]) {
        [self.mapView gm_RemoveAnnotations:annotations];
    }
    [self.annoArray removeObjectsInArray:annotations];
}

- (id <GMAnnotationViewDelegate>)gm_ViewForAnnotation:(id <GMAnnotation>)annotation
{
    return [self.mapView gm_ViewForAnnotation:annotation];
}

- (void)gm_SelectAnnotation:(id <GMAnnotation>)annotation animated:(BOOL)animated;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_SelectAnnotation:animated:)]) {
        [self.mapView gm_SelectAnnotation:annotation animated:animated];
    }
}

- (void)gm_DeselectAnnotation:(id <GMAnnotation>)annotation animated:(BOOL)animated;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_DeselectAnnotation:animated:)]) {
        [self.mapView gm_DeselectAnnotation:annotation animated:animated];
    }
}

- (void)gm_ShowAnnotations:(NSArray *)annotations animated:(BOOL)animated;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_ShowAnnotations:animated:)]) {
        [self.mapView gm_ShowAnnotations:annotations animated:animated];
    }
}

- (void)gm_SetCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_SetCenterCoordinate:animated:)]) {
        [self.mapView gm_SetCenterCoordinate:coordinate animated:animated];
    }
}

- (void)gm_AddOverlay:(id<GMOverlay>)overlay
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_AddOverlay:)]) {
        [self.mapView gm_AddOverlay:overlay];
    }
    
    [self.overlayArray addObject:overlay];
}

- (void)gm_AddOverlays:(NSArray *)overlays;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_AddOverlays:)]) {
        [self.mapView gm_AddOverlays:overlays];
    }
    
    [self.overlayArray addObjectsFromArray:overlays];
}

- (void)gm_RemoveOverlay:(id<GMOverlay>)overlay;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_RemoveOverlay:)]) {
        [self.mapView gm_RemoveOverlay:overlay];
    }
    
    [self.overlayArray removeObject:overlay];
}

- (void)gm_RemoveOverlays:(NSArray *)overlays;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_RemoveOverlays:)]) {
        [self.mapView gm_RemoveOverlays:overlays];
    }
    
    [self.overlayArray removeObjectsInArray:overlays];
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay atIndex:(NSUInteger)index;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_InsertOverlay:atIndex:)]) {
        [self.mapView gm_InsertOverlay:overlay atIndex:index];
    }
}

- (void)gm_ExchangeOverlayAtIndex:(NSUInteger)index1 withOverlayAtIndex:(NSUInteger)index2;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_ExchangeOverlayAtIndex:withOverlayAtIndex:)]) {
        [self.mapView gm_ExchangeOverlayAtIndex:index1 withOverlayAtIndex:index2];
    }
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay aboveOverlay:(id<GMOverlay>)sibling;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_InsertOverlay:aboveOverlay:)]) {
        [self.mapView gm_InsertOverlay:overlay aboveOverlay:sibling];
    }
}

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay belowOverlay:(id<GMOverlay>)sibling;
{
    if (self.mapView && [self.mapView respondsToSelector:@selector(gm_InsertOverlay:belowOverlay:)]) {
        [self.mapView gm_InsertOverlay:overlay belowOverlay:sibling];
    }
}

/**
 *将View坐标转换成经纬度坐标
 *@param point 待转换的View坐标
 *@param view point坐标所在的view
 *@return 转换后的经纬度坐标
 */
- (CLLocationCoordinate2D)convertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view
{
    return [self.mapView gm_ConvertPoint:point toCoordinateFromView:view];
}

@end
