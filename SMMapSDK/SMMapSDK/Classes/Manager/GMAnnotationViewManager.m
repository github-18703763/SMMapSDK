//
//  GMAnnotationViewManager.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/12.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMAnnotationViewManager.h"
#import "GM_BMKAnnotationView.h"
#import "GM_MAAnnotationView.h"
#import "GM_QAnnotationView.h"
#import "GM_MKAnnotationView.h"

@implementation GMAnnotationViewManager

+ (instancetype)shareInstance
{
    static GMAnnotationViewManager * manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[GMAnnotationViewManager alloc] init];
        manager.canShowCallout = YES;
        manager.draggable = YES;
        manager.animateDrop = YES;
        manager.enabled = YES;
        manager.centerOffset = CGPointMake(0, 0);
        manager.calloutOffset = CGPointMake(0, 0);
        manager.selected = NO;
        manager.draggable = NO;
    });
    return manager;
}

- (id<GMAnnotationViewDelegate>)mapView:(id<GMMapViewDelegate>)mapView annotation:(id<GMAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
#ifdef __SMap__
    if ([mapView isKindOfClass:[GM_MKMapView class]]) {
        GM_MKAnnotationView * gm_mkAnnotationView;
        if (mapView && [mapView respondsToSelector:@selector(dequeueReusableCellWithIdentifier:)]) {
            gm_mkAnnotationView = (GM_MKAnnotationView *)[mapView gm_dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
        }
        if (!gm_mkAnnotationView) {
            gm_mkAnnotationView = [[GM_MKAnnotationView alloc] initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:reuseIdentifier];
        }
        self.annoDelegate = gm_mkAnnotationView;
        gm_mkAnnotationView.canShowCallout = self.canShowCallout;
        gm_mkAnnotationView.animatesDrop = self.animateDrop;
        gm_mkAnnotationView.draggable = self.draggable;
        gm_mkAnnotationView.image = self.image;
        gm_mkAnnotationView.enabled = self.enabled;
        gm_mkAnnotationView.centerOffset = self.centerOffset;
        gm_mkAnnotationView.calloutOffset = self.calloutOffset;
        gm_mkAnnotationView.selected = self.selected;
//        gm_mkAnnotationView.transform = CGAffineTransformMakeRotation(M_PI_4);
        return gm_mkAnnotationView;
    }
#endif
    
#ifdef __AMap__
    else if ([mapView isKindOfClass:[GM_MAMapView class]]) {
        GM_MAAnnotationView * gm_maAnnotationView;
        if (mapView && [mapView respondsToSelector:@selector(dequeueReusableCellWithIdentifier:)]) {
            gm_maAnnotationView = (GM_MAAnnotationView *)[mapView gm_dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
        }
        if (!gm_maAnnotationView) {
            gm_maAnnotationView = [[GM_MAAnnotationView alloc] initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:reuseIdentifier];
        }
        self.annoDelegate = gm_maAnnotationView;
        gm_maAnnotationView.canShowCallout = self.canShowCallout;
        gm_maAnnotationView.draggable = self.draggable;
        gm_maAnnotationView.image = self.image;
        gm_maAnnotationView.animatesDrop = self.animateDrop;
        gm_maAnnotationView.enabled = self.enabled;
        gm_maAnnotationView.centerOffset = self.centerOffset;
        gm_maAnnotationView.calloutOffset = self.calloutOffset;
        gm_maAnnotationView.selected = self.selected;
        gm_maAnnotationView.transform = CGAffineTransformMakeRotation(M_PI_4);
        return gm_maAnnotationView;
    }
    
#endif
    
#ifdef __BMK__
    else if ([mapView isKindOfClass:[GM_BMKMapView class]]) {
        GM_BMKAnnotationView * gm_bmkAnnotationView;
        if (mapView && [mapView respondsToSelector:@selector(dequeueReusableCellWithIdentifier:)]) {
            gm_bmkAnnotationView = (GM_BMKAnnotationView *)[mapView gm_dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
        }
        if (!gm_bmkAnnotationView) {
            gm_bmkAnnotationView = [[GM_BMKAnnotationView alloc] initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:reuseIdentifier];
        }
        self.annoDelegate = gm_bmkAnnotationView;
        gm_bmkAnnotationView.canShowCallout = self.canShowCallout;
        gm_bmkAnnotationView.draggable = self.draggable;
        gm_bmkAnnotationView.image = self.image;
        gm_bmkAnnotationView.animatesDrop = self.animateDrop;
        gm_bmkAnnotationView.enabled = self.enabled;
        gm_bmkAnnotationView.centerOffset = self.centerOffset;
        gm_bmkAnnotationView.calloutOffset = self.calloutOffset;
        gm_bmkAnnotationView.selected = self.selected;
        gm_bmkAnnotationView.transform = CGAffineTransformMakeRotation(M_PI_4);
        return gm_bmkAnnotationView;
    }
    
#endif
    
#ifdef __QMap__
    else if ([mapView isKindOfClass:[GM_QMapView class]]) {
        GM_QAnnotationView * gm_qAnnotationView;
        if (mapView && [mapView respondsToSelector:@selector(dequeueReusableCellWithIdentifier:)]) {
            gm_qAnnotationView = (GM_QAnnotationView *)[mapView gm_dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
        }
        if (!gm_qAnnotationView) {
            gm_qAnnotationView = [[GM_QAnnotationView alloc] initWithAnnotation:(id<QAnnotation>)annotation reuseIdentifier:reuseIdentifier];
        }
        self.annoDelegate = gm_qAnnotationView;
        gm_qAnnotationView.canShowCallout = self.canShowCallout;
        gm_qAnnotationView.draggable = self.draggable;
        gm_qAnnotationView.image = self.image;
        gm_qAnnotationView.animatesDrop = self.animateDrop;
        gm_qAnnotationView.enabled = self.enabled;
        gm_qAnnotationView.centerOffset = self.centerOffset;
        gm_qAnnotationView.calloutOffset = self.calloutOffset;
        gm_qAnnotationView.selected = self.selected;
//        gm_qAnnotationView.transform = CGAffineTransformMakeRotation(M_PI_4);
        return gm_qAnnotationView;
    }
#endif
    
#ifdef __GoogleMap__
    
#endif
    
    return nil;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [self.annoDelegate gm_SetSelected:selected animated:animated];
}

@end
