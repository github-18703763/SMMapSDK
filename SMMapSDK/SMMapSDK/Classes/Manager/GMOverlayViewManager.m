//
//  GMOverlayViewManager.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMOverlayViewManager.h"

#import "GM_BMKCircle.h"
#import "GM_MKCircle.h"
#import "GM_MACircle.h"
#import "GM_QCircle.h"

#import "GM_BMKCirecleView.h"
#import "GM_QCircleView.h"
#import "GM_MKCircleView.h"
#import "GM_MACircleView.h"

#import "GM_BMKPolylineView.h"
#import "GM_MAPolylineView.h"
#import "GM_MKPolylineRenderer.h"
#import "GM_QPolylineView.h"

#import "GM_BMKPolygonView.h"
#import "GM_MKPolygonRenderer.h"
#import "GM_MAPolygonView.h"
#import "GM_QPolygonView.h"

@implementation GMOverlayViewManager

+ (instancetype)shareInstance
{
    static GMOverlayViewManager * manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[GMOverlayViewManager alloc] init];
        manager.lineJoinType = kCGLineJoinBevel;
        manager.lineCapType = kCGLineCapButt;
    });
    return manager;
}

- (id<GMOverlayViewDelegate>)mapView:(id<GMMapViewDelegate>)mapView overlay:(id<GMOverlay>)overlay
{
#ifdef __SMap__
    if ([mapView isKindOfClass:[GM_MKMapView class]]) {
        if ([overlay isKindOfClass:[MKCircle class]]) {
            GM_MKCircleView * gm_mkCircleView = [[GM_MKCircleView alloc] initWithCircle:overlay];
            gm_mkCircleView.fillColor = self.fillColor;
            gm_mkCircleView.strokeColor = self.strokeColor;
            gm_mkCircleView.lineWidth = self.lineWidth;
            gm_mkCircleView.lineCap = self.lineCapType;
            gm_mkCircleView.lineJoin = self.lineJoinType;
            return gm_mkCircleView;
        }
        if ([overlay isKindOfClass:[MKPolyline class]]) {
            GM_MKPolylineRenderer * gm_mkPolylineView = [[GM_MKPolylineRenderer alloc] initWithPolyline:overlay];
            gm_mkPolylineView.fillColor = self.fillColor;
            gm_mkPolylineView.strokeColor = self.strokeColor;
            gm_mkPolylineView.lineWidth = self.lineWidth;
            gm_mkPolylineView.lineCap = self.lineCapType;
            gm_mkPolylineView.lineJoin = self.lineJoinType;
            return gm_mkPolylineView;
        }
        if ([overlay isKindOfClass:[MKPolygon class]]) {
            GM_MKPolygonRenderer * gm_mkPolygonView = [[GM_MKPolygonRenderer alloc] initWithPolygon:overlay];
            gm_mkPolygonView.fillColor = self.fillColor;
            gm_mkPolygonView.strokeColor = self.strokeColor;
            gm_mkPolygonView.lineWidth = self.lineWidth;
            gm_mkPolygonView.lineCap = self.lineCapType;
            gm_mkPolygonView.lineJoin = self.lineJoinType;
            return gm_mkPolygonView;
        }
    }
#endif
    
#ifdef __AMap__
    else if ([mapView isKindOfClass:[GM_MAMapView class]]) {
        if ([overlay isKindOfClass:[MACircle class]]) {
            GM_MACircleView * gm_maCircleView = [[GM_MACircleView alloc] initWithCircle:overlay];
            gm_maCircleView.fillColor = self.fillColor;
            gm_maCircleView.strokeColor = self.strokeColor;
            gm_maCircleView.lineWidth = self.lineWidth;
            gm_maCircleView.lineCap = self.lineCapType;
            gm_maCircleView.lineJoin = self.lineJoinType;
            return gm_maCircleView;
        }
        if ([overlay isKindOfClass:[MAPolyline class]]) {
            GM_MAPolylineView * gm_maPolylineView = [[GM_MAPolylineView alloc] initWithPolyline:overlay];
            gm_maPolylineView.fillColor = self.fillColor;
            gm_maPolylineView.strokeColor = self.strokeColor;
            gm_maPolylineView.lineWidth = self.lineWidth;
            gm_maPolylineView.lineJoin = self.lineJoinType;
            gm_maPolylineView.lineCap = self.lineCapType;
            return gm_maPolylineView;
        }
        if ([overlay isKindOfClass:[MAPolygon class]]) {
            GM_MAPolygonView * gm_maPolygonView = [[GM_MAPolygonView alloc] initWithPolygon:overlay];
            gm_maPolygonView.fillColor = self.fillColor;
            gm_maPolygonView.strokeColor = self.strokeColor;
            gm_maPolygonView.lineWidth = self.lineWidth;
            gm_maPolygonView.lineJoin = self.lineJoinType;
            gm_maPolygonView.lineCap = self.lineCapType;
            return gm_maPolygonView;
        }
    }
    
#endif
    
#ifdef __BMK__
    else if ([mapView isKindOfClass:[GM_BMKMapView class]]) {
        if ([overlay isKindOfClass:[BMKCircle class]]) {
            GM_BMKCirecleView * gm_bmkCircleView = [[GM_BMKCirecleView alloc] initWithCircle:(BMKCircle *)overlay];
            gm_bmkCircleView.fillColor = self.fillColor;
            gm_bmkCircleView.strokeColor = self.strokeColor;
            gm_bmkCircleView.lineWidth = self.lineWidth;
            gm_bmkCircleView.lineDash = self.lineDash;
            return gm_bmkCircleView;
        }
        if ([overlay isKindOfClass:[BMKPolyline class]]) {
            GM_BMKPolylineView * gm_bmkPolylineView = [[GM_BMKPolylineView alloc] initWithPolyline:overlay];
            gm_bmkPolylineView.fillColor = self.fillColor;
            gm_bmkPolylineView.strokeColor = self.strokeColor;
            gm_bmkPolylineView.lineWidth = self.lineWidth;
            gm_bmkPolylineView.lineDash = self.lineDash;
            return gm_bmkPolylineView;
        }
        if ([overlay isKindOfClass:[BMKPolygon class]]) {
            GM_BMKPolygonView * gm_bmkPolygonView = [[GM_BMKPolygonView alloc] initWithPolygon:overlay];
            gm_bmkPolygonView.fillColor = self.fillColor;
            gm_bmkPolygonView.strokeColor = self.strokeColor;
            gm_bmkPolygonView.lineWidth = self.lineWidth;
            gm_bmkPolygonView.lineDash = self.lineDash;
            return gm_bmkPolygonView;
        }
    }
    
#endif
    
#ifdef __QMap__
    else if ([mapView isKindOfClass:[GM_QMapView class]]) {
        if ([overlay isKindOfClass:[QCircle class]]) {
            GM_QCircleView * gm_qCircleView = [[GM_QCircleView alloc] initWithCircle:(QCircle *)overlay];
            gm_qCircleView.fillColor = self.fillColor;
            gm_qCircleView.strokeColor = self.strokeColor;
            gm_qCircleView.lineWidth = self.lineWidth;
            return gm_qCircleView;
        }
        if ([overlay isKindOfClass:[QPolyline class]]) {
            GM_QPolylineView * gm_qPolylineView = [[GM_QPolylineView alloc] initWithPolyline:overlay];
            gm_qPolylineView.fillColor = self.fillColor;
            gm_qPolylineView.strokeColor = self.strokeColor;
            gm_qPolylineView.lineWidth = self.lineWidth;
            return gm_qPolylineView;
        }
        if ([overlay isKindOfClass:[QPolygon class]]) {
            GM_QPolygonView * gm_qPolygonView = [[GM_QPolygonView alloc] initWithPolygon:overlay];
            gm_qPolygonView.fillColor = self.fillColor;
            gm_qPolygonView.strokeColor = self.strokeColor;
            gm_qPolygonView.lineWidth = self.lineWidth;
            return gm_qPolygonView;
        }
    }
#endif
    
#ifdef __GoogleMap__
    
#endif
    
    return nil;
}

@end
