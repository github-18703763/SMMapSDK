//
//  GMMapViewDelegate.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/10.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMAnnotation.h"
#import "GMAnnotationViewDelegate.h"
#import "GMOverlay.h"
#import "GMOverlayViewDelegate.h"

@protocol GMMapViewDelegate <NSObject>

#pragma mark - 地图自身实现的协议

#pragma mark - 地图操作
@optional
/**
 *  地图放大
 */
- (void)gm_ZoomIn;

/**
 *  地图缩小
 */
- (void)gm_ZoomOut;

- (void)gm_setMapType:(NSInteger)mapType;

/**
 *  设置中心点
 *
 *  @param coordinate 中心点坐标
 *  @param animated   是否添加动画效果
 */
- (void)gm_SetCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated;

#pragma mark - Annotations
/**
 *  添加标注
 *
 *  @param annotation 遵守GMAnnotation协议的标注
 */
- (void)gm_AddAnnotation:(id<GMAnnotation>)annotation;

/**
 *  添加一组标注
 *
 *  @param annotations 一组标注
 */
- (void)gm_AddAnnotations:(NSArray *)annotations;

/**
 *  删除标注
 *
 *  @param annotation 遵守GMAnnotation协议的标注
 */
- (void)gm_RemoveAnnotation:(id<GMAnnotation>)annotation;

/**
 *  删除一组标注
 *
 *  @param annotations 一组标注
 */
- (void)gm_RemoveAnnotations:(NSArray *)annotations;

/**
 *  返回显示的标注
 *
 *  @param annotation 标注
 *
 *  @return 标注视图
 */
- (id <GMAnnotationViewDelegate>)gm_ViewForAnnotation:(id <GMAnnotation>)annotation;

/**
 *  选中标注
 *
 *  @param annotation 选中的标注
 *  @param animated   是否有动画效果
 */
- (void)gm_SelectAnnotation:(id <GMAnnotation>)annotation animated:(BOOL)animated;

/**
 *  取消选中标注
 *
 *  @param annotation 取消选中的标注
 *  @param animated   是否动画效果
 */
- (void)gm_DeselectAnnotation:(id <GMAnnotation>)annotation animated:(BOOL)animated;

/**
 *  显示一组标注
 *
 *  @param annotations 一组标注
 *  @param animated    是否有动画效果
 */
- (void)gm_ShowAnnotations:(NSArray *)annotations animated:(BOOL)animated;

/**
 *  根据指定标识查找一个可被复用的标注View，一般在delegate中使用，用此函数来代替新申请一个View
 *
 *  @param identifier identifier 指定标识
 *
 *  @return 返回可被复用的标注View
 */
- (id<GMAnnotationViewDelegate> )gm_dequeueReusableAnnotationViewWithIdentifier:(NSString *)identifier;

#pragma mark - Overlay

- (void)gm_AddOverlay:(id<GMOverlay>)overlay;

- (void)gm_AddOverlays:(NSArray *)overlays;

- (void)gm_RemoveOverlay:(id<GMOverlay>)overlay;

- (void)gm_RemoveOverlays:(NSArray *)overlays;

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay atIndex:(NSUInteger)index;

- (void)gm_ExchangeOverlayAtIndex:(NSUInteger)index1 withOverlayAtIndex:(NSUInteger)index2;

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay aboveOverlay:(id<GMOverlay>)sibling;

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay belowOverlay:(id<GMOverlay>)sibling;

- (CLLocationCoordinate2D)gm_ConvertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view;


#pragma mark - 外部实现的协议

- (id<GMAnnotationViewDelegate>)gm_mapView:(id<GMMapViewDelegate>)mapView viewForAnnotation:(id<GMAnnotation>)annotation;

- (void)gm_mapView:(id<GMMapViewDelegate>)mapView didSelectAnnotationView:(id<GMAnnotationViewDelegate>)view;

- (void)gm_mapView:(id<GMMapViewDelegate>)mapView didDeselectAnnotationView:(id<GMAnnotationViewDelegate>)view;

- (id<GMOverlayViewDelegate>)gm_mapView:(id<GMMapViewDelegate>)mapView viewForOverlay:(id<GMOverlay>)overlay;

- (void)gm_mapView:(id<GMMapViewDelegate>)mapView didAddAnnotationViews:(NSArray *)views;

- (void)gm_mapView:(id<GMMapViewDelegate>)mapView annotationViewForBubble:(id<GMAnnotationViewDelegate>)view;

- (void)gm_mapView:(id<GMMapViewDelegate>)mapView didAddOverlayViews:(NSArray *)overlayViews;

/*!
 @brief 地图区域即将改变时会调用此接口
 @param mapview 地图View
 @param animated 是否动画
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView regionWillChangeAnimated:(BOOL)animated;

/*!
 @brief 地图区域改变完成后会调用此接口
 @param mapview 地图View
 @param animated 是否动画
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView regionDidChangeAnimated:(BOOL)animated;

/**
 *  地图将要发生移动时调用此接口
 *
 *  @param mapView       地图view
 *  @param wasUserAction 标识是否是用户动作
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView mapWillMoveByUser:(BOOL)wasUserAction;

/**
 *  地图移动结束后调用此接口
 *
 *  @param mapView       地图view
 *  @param wasUserAction 标识是否是用户动作
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView mapDidMoveByUser:(BOOL)wasUserAction;

/**
 *  地图将要发生缩放时调用此接口
 *
 *  @param mapView       地图view
 *  @param wasUserAction 标识是否是用户动作
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView mapWillZoomByUser:(BOOL)wasUserAction;

/**
 *  地图缩放结束后调用此接口
 *
 *  @param mapView       地图view
 *  @param wasUserAction 标识是否是用户动作
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView mapDidZoomByUser:(BOOL)wasUserAction;

/**
 *  单击地图底图调用此接口
 *
 *  @param mapView    地图View
 *  @param coordinate 点击位置经纬度
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView didSingleTappedAtCoordinate:(CLLocationCoordinate2D)coordinate;

/**
 *  长按地图底图调用此接口
 *
 *  @param mapView    地图View
 *  @param coordinate 长按位置经纬度
 */
- (void)gm_MapView:(id<GMMapViewDelegate>)mapView didLongPressedAtCoordinate:(CLLocationCoordinate2D)coordinate;

@end
