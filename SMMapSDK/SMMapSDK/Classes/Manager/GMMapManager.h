//
//  GMMapManager.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/9.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMMapViewDelegate.h"

static NSString *const kBMKMapKey         = @"em22Ot1WigAjbh4B8p0GmA67kkEIFaay";
static NSString *const kMAMapKey          = @"6aff87963f11fd5625b4d9604f5d9374";
static NSString *const kTencentMapKey     = @"42SBZ-HKU33-7H63D-35UQA-PCSOO-5MB6B";
static NSString *const kGoogleMapKey      = @"AIzaSyA7gpcXuhO0EpBo4srpvcTip3k15OgqI-4";

typedef NS_ENUM (NSUInteger, GMMapViewType){
    GMMapViewTypeSystemMap  = 1,   // 系统自带地图
    GMMapViewTypeMAMap,            // 高德地图
    GMMapViewTypeBMKMap,           // 百度地图
    GMMapViewTypeQMap,             // 腾讯地图
    GMMapViewTypeGoogleMap,        // 谷歌地图
    GMMapViewTypeNone
};

typedef NS_ENUM (NSUInteger, GMMapType){
    GMMapTypeStandard  = 0,   // 标准地图
    GMMapTypeSatellite,       // 卫星地图
};

@protocol GMMapManagerDelegate <NSObject>

@required
/**
 *  询问代理获取地图的父view
 *
 *  @return 返回父view
 */
- (UIView *)superViewForMapView;

/**
 *  返回地图的代理
 *
 *  @return 代理
 */
- (id)anyDelegate;

@optional
- (void)mapViewDidChange:(GMMapViewType)mapViewType;

@end

@interface GMMapManager : NSObject

@property (nonatomic, assign) GMMapType mapType;

@property (nonatomic, assign) GMMapViewType mapViewType;

@property (nonatomic, assign) CLLocationCoordinate2D centerCoordinate;

@property (nonatomic, weak) id<GMMapManagerDelegate> delegate;

@property (nonatomic, weak) id<GMMapViewDelegate> mapView;

@property (nonatomic, strong) NSMutableArray < id<GMAnnotation> > * annoArray;

@property (nonatomic, strong) NSMutableArray < id<GMOverlay> > * overlayArray;

/**
 *  注册高德地图key
 */
+ (void)registerAMapKey;

/**
 *  注册百度地图key
 */
+ (void)registerBMKMapKey;

/**
 *  注册腾讯地图key
 */
+ (void)registerQMapKey;

/**
 *  注册谷歌地图key
 */
+ (void)registerGMSMapKey;

/**
 *  地图管理单利创建 (考虑到多界面 地图为同一类型 故不用单例)
 *
 *  @return 返回实例
 */
//+ (instancetype)shareManager;

#pragma mark - MapViewOperation
/**
 *  地图放大
 */
- (void)zoomIn;

/**
 *  地图缩小
 */
- (void)zoomOut;

- (void)gm_SetCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated;


#pragma mark - Annotation

- (void)gm_AddAnnotation:(id<GMAnnotation>)annotation;

- (void)gm_AddAnnotations:(NSArray *)annotations;

- (void)gm_RemoveAnnotation:(id<GMAnnotation>)annotation;

- (void)gm_RemoveAnnotations:(NSArray *)annotations;

- (id<GMAnnotationViewDelegate>)gm_ViewForAnnotation:(id<GMAnnotation>)annotation;

- (void)gm_SelectAnnotation:(id<GMAnnotation>)annotation animated:(BOOL)animated;

- (void)gm_DeselectAnnotation:(id<GMAnnotation>)annotation animated:(BOOL)animated;

- (void)gm_ShowAnnotations:(NSArray *)annotations animated:(BOOL)animated;

#pragma mark - Overlay

- (void)gm_AddOverlay:(id<GMOverlay>)overlay;

- (void)gm_AddOverlays:(NSArray *)overlays;

- (void)gm_RemoveOverlay:(id<GMOverlay>)overlay;

- (void)gm_RemoveOverlays:(NSArray *)overlays;

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay atIndex:(NSUInteger)index;

- (void)gm_ExchangeOverlayAtIndex:(NSUInteger)index1 withOverlayAtIndex:(NSUInteger)index2;

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay aboveOverlay:(id<GMOverlay>)sibling;

- (void)gm_InsertOverlay:(id<GMOverlay>)overlay belowOverlay:(id<GMOverlay>)sibling;

#pragma mark - 关于坐标转换
/**
 *将View坐标转换成经纬度坐标
 *@param point 待转换的View坐标
 *@param view point坐标所在的view
 *@return 转换后的经纬度坐标
 */
- (CLLocationCoordinate2D)convertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view;

@end
