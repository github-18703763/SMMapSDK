//
//  GMOverlayViewGMnager.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/13.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <Foundation/Foundation.h>

enum GMLineJoinType
{
    kGMLineJoinBevel,   //!< 斜面连接点
    kGMLineJoinMiter,   //!< 斜接连接点
    kGMLineJoinRound    //!< 圆角连接点
};
typedef enum GMLineJoinType GMLineJoinType;

enum GMLineCapType
{
    kGMLineCapButt,     //!< 普通头
    kGMLineCapSquare,   //!< 扩展头
    kGMLineCapArrow,    //!< 箭头
    kGMLineCapRound     //!< 圆形头
};
typedef enum GMLineCapType GMLineCapType;

@interface GMOverlayViewManager : NSObject

/// 填充颜色
/// 注：请使用 - (UIColor *)initWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha; 初始化UIColor，使用[UIColor ***Color]初始化时，个别case转换成RGB后会有问题
@property (strong, nonatomic) UIColor *fillColor;
/// 画笔颜色
/// 注：请使用 - (UIColor *)initWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha; 初始化UIColor，使用[UIColor ***Color]初始化时，个别case转换成RGB后会有问题
@property (strong, nonatomic) UIColor *strokeColor;
/// 画笔宽度，默认为0
@property  (nonatomic) CGFloat lineWidth;
/// path对象
@property CGPathRef path;
/// 是否为虚线样式，默认NO
@property (nonatomic) BOOL lineDash;
/// 是否纹理图片平铺绘制，默认NO
@property (assign, nonatomic) BOOL tileTexture;
/// 纹理图片是否缩放（tileTexture为YES时生效），默认NO
@property (assign, nonatomic) BOOL keepScale;

/**
 *  LineJoin,默认是kGMLineJoinBevel
 */
@property CGLineJoin lineJoinType;

/**
 *  LineCap,默认是kGMLineCapButt
 */
@property CGLineCap lineCapType;

/**
 *  MiterLimit,默认是10.f
 */
@property CGFloat miterLimit;

+ (instancetype)shareInstance;

- (id<GMOverlayViewDelegate>)mapView:(id<GMMapViewDelegate>)mapView overlay:(id<GMOverlay>)overlay;

@end
