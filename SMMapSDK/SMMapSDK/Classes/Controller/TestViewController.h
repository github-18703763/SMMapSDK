//
//  TestViewController.h
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/12.
//  Copyright © 2016年 goome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMMapManager.h"

@interface TestViewController : UIViewController

@property (nonatomic, assign) GMMapViewType mapType;

@end
