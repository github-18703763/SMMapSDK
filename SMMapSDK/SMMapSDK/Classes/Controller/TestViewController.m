//
//  TestViewController.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/12.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "TestViewController.h"
#import "GMSettingView.h"
#import <QMapKit/QMapView.h>

/**
 Test
 */
@interface TestViewController () <GMMapManagerDelegate, GMSetViewDelegate>
@property (nonatomic, strong) GMMapManager * mapManager;
@property (nonatomic, strong) GMSettingView * settingView;
@property (nonatomic, strong) UIButton * setBtn;

@end

@implementation TestViewController

- (GMMapManager *)mapManager
{
    if (!_mapManager) {
        _mapManager = [[GMMapManager alloc] init];
        _mapManager.delegate = self;
    }
    return _mapManager;
}

- (GMSettingView *)settingView
{
    if (!_settingView) {
        _settingView = [[GMSettingView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 150)];
        _settingView.backgroundColor = [UIColor lightGrayColor];
        _settingView.selectIndex = 2;
        _settingView.delegate = self;
        [self.view addSubview:_settingView];
    }
    return _settingView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.mapManager.mapViewType = self.mapType;
    
    _setBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [_setBtn setTitle:@"设置" forState:UIControlStateNormal];
    [_setBtn setTitle:@"完成" forState:UIControlStateSelected];
    [_setBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_setBtn addTarget:self action:@selector(set:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_setBtn];
    
    self.settingView.hidden = YES;
}

- (void)set:(UIButton *)btn
{
    btn.selected = !btn.selected;
    self.settingView.hidden = !btn.selected;
}

#pragma mark - GMMapManagerDelegate

- (UIView *)superViewForMapView
{
    return self.view;
}

- (id)anyDelegate
{
    return self;
}

#pragma mark - GMSetViewDelegate
- (void)changeMapViewIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            self.mapManager.mapViewType = GMMapViewTypeSystemMap;
            break;
        case 1:
            self.mapManager.mapViewType = GMMapViewTypeMAMap;
            break;
        case 2:
            self.mapManager.mapViewType = GMMapViewTypeBMKMap;
            break;
        case 3:
            self.mapManager.mapViewType = GMMapViewTypeQMap;
            break;
        case 4:
            self.mapManager.mapViewType = GMMapViewTypeGoogleMap;
            break;
        default:
            break;
    }
}

- (void)mapViewZoomIn
{
    [self.mapManager zoomIn];
}

- (void)mapViewZoonOut
{
    [self.mapManager zoomOut];
}

- (void)mapViewAddAnnotation
{
}

- (void)mapViewRemoveAnnotation
{
    
}

- (void)mapViewAddOverlay
{
    
}

- (void)mapViewRemoveOverlay
{
    
}

- (void)mapViewMoveCenterCoor
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
