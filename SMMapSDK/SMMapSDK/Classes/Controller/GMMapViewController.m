//
//  GMMapViewController.m
//  GMMapSDKTest
//
//  Created by zhuch on 16/8/9.
//  Copyright © 2016年 goome. All rights reserved.
//

#import "GMMapViewController.h"
#import "GMMapManager.h"
#import "GMSettingView.h"
#import "GMPointAnnotation.h"
#import "GMAnnotationViewManager.h"
#import "TestViewController.h"
#import "GMCircle.h"
#import "GMPolyline.h"
#import "GMPolygon.h"
#import "GMOverlayViewManager.h"

#import "AppDelegate.h"

@interface GMMapViewController () <GMMapManagerDelegate, GMSetViewDelegate, GMMapViewDelegate>
@property (nonatomic, strong) UIButton * setBtn;
@property (nonatomic, strong) GMSettingView * settingView;

@property (nonatomic, strong) GMPointAnnotation * pointAnnotation;
@property (nonatomic, strong) GMMapManager * mapManager;

@property (nonatomic, strong) NSMutableArray <id<GMAnnotation>>* annotationArray;
@property (nonatomic,strong) NSMutableArray <id<GMOverlay>>* overlayArray;

@end

@implementation GMMapViewController

- (GMMapManager *)mapManager
{
    if (!_mapManager) {
        _mapManager = [[GMMapManager alloc] init];
    }
    return _mapManager;
}

- (GMSettingView *)settingView
{
    if (!_settingView) {
        _settingView = [[GMSettingView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 150)];
        _settingView.backgroundColor = [UIColor lightGrayColor];
        _settingView.selectIndex = 0;
        _settingView.delegate = self;
        [self.view addSubview:_settingView];
    }
    return _settingView;
}

- (NSMutableArray *)annotationArray
{
    if (!_annotationArray) {
        _annotationArray = [NSMutableArray array];
    }
    return _annotationArray;
}

- (NSMutableArray *)overlayArray
{
    if (!_overlayArray) {
        _overlayArray = [NSMutableArray array];
    }
    return _overlayArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationController.navigationBar.translucent = NO;
    
    AppDelegate * appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.mapManager.delegate = self;
    self.mapManager.mapViewType = appD.mapViewType;
    
    _setBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [_setBtn setTitle:@"设置" forState:UIControlStateNormal];
    [_setBtn setTitle:@"完成" forState:UIControlStateSelected];
    [_setBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_setBtn addTarget:self action:@selector(set:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_setBtn];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(push)];
    
    self.settingView.hidden = YES;
}

- (void)push
{
    TestViewController * testVc = [[TestViewController alloc] init];
    testVc.mapType = self.mapManager.mapViewType;
    [self.navigationController pushViewController:testVc animated:YES];
}

- (void)set:(UIButton *)btn
{
    btn.selected = !btn.selected;
    self.settingView.hidden = !btn.selected;
}

#pragma mark - GMMapManagerDelegate
- (UIView *)superViewForMapView
{
    return self.view;
}

- (id)anyDelegate
{
    return self;
}

- (void)mapViewDidChange:(GMMapViewType)mapViewType
{
    switch (mapViewType) {
        case GMMapViewTypeSystemMap:
            self.title = @"系统地图";
            break;
        case GMMapViewTypeMAMap:
            self.title = @"高德地图";
            break;
        case GMMapViewTypeBMKMap:
            self.title = @"百度地图";
            break;
        case GMMapViewTypeQMap:
            self.title = @"腾讯地图";
            break;
        case GMMapViewTypeGoogleMap:
            self.title = @"谷歌地图";
            break;
        default:
            break;
    }
}

#pragma mark - GMSetViewDelegate
- (void)changeMapViewIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            self.mapManager.mapViewType = GMMapViewTypeSystemMap;
            break;
        case 1:
            self.mapManager.mapViewType = GMMapViewTypeMAMap;
            break;
        case 2:
            self.mapManager.mapViewType = GMMapViewTypeBMKMap;
            break;
        case 3:
            self.mapManager.mapViewType = GMMapViewTypeQMap;
            break;
        case 4:
            self.mapManager.mapViewType = GMMapViewTypeGoogleMap;
            break;
        default:
            break;
    }
}

- (void)mapViewZoomIn
{
    [self.mapManager zoomIn];
}

- (void)mapViewZoonOut
{
    [self.mapManager zoomOut];
}

- (void)mapViewAddAnnotation
{
    static NSInteger i = 1;
    _pointAnnotation = [[GMPointAnnotation alloc] init];
    _pointAnnotation.title = [NSString stringWithFormat:@"北京天安门广场%ld",i];
    _pointAnnotation.subtitle = [NSString stringWithFormat:@"我爱北京天安门+%ld",i];
    _pointAnnotation.coordinate = [self.mapManager convertPoint:[self randomPoint] toCoordinateFromView:self.view];
    [self.annotationArray addObject:_pointAnnotation];
    [self.mapManager gm_AddAnnotation:_pointAnnotation];
    i++;
}

- (void)mapViewRemoveAnnotation
{
    if (self.annotationArray.count == 0) {
        return;
    }
    [self.mapManager gm_RemoveAnnotations:self.annotationArray];
    [self.annotationArray removeAllObjects];
}

- (void)mapViewAddOverlay
{
    // 圆
    GMCircle * circle = [GMCircle circleWithCenterCoordinate:CLLocationCoordinate2DMake(39.952136, 116.50095) radius:5000];
    [self.overlayArray addObject:circle.cirDelegate];
    
    // 线
    CLLocationCoordinate2D commonPolylineCoords[5];
    commonPolylineCoords[0].latitude = 39.832136;
    commonPolylineCoords[0].longitude = 116.34095;
    
    commonPolylineCoords[1].latitude = 39.832136;
    commonPolylineCoords[1].longitude = 116.42095;
    
    commonPolylineCoords[2].latitude = 39.902136;
    commonPolylineCoords[2].longitude = 116.42095;
    
    commonPolylineCoords[3].latitude = 39.902136;
    commonPolylineCoords[3].longitude = 116.44095;
    
    commonPolylineCoords[4].latitude = 39.932136;
    commonPolylineCoords[4].longitude = 116.44095;
    
    GMPolyline * polyline = [GMPolyline polylineWithCoordinates:commonPolylineCoords count:5];
//    id<GMPolylineDelegate> line = [polyline polylineWithMapType:self.mapManager.mapViewType];
//    [self.mapManager gm_AddOverlay:line];
    [self.overlayArray addObject:polyline.polylineDelegate];
    
    // 多边形
#define STAR_RAYS_COUNT 5 //绘制多角星。 STAR_RAYS_COUNT为5时即为五角星
    CLLocationCoordinate2D coordinates[STAR_RAYS_COUNT * 2];
    [self generateStarPoints:coordinates pointsCount:STAR_RAYS_COUNT * 2 atCenter:CLLocationCoordinate2DMake(39.800892, 116.293413)];//生成多角星的坐标
    GMPolygon *polygon = [GMPolygon polygonWithCoordinates:coordinates count:STAR_RAYS_COUNT * 2];
//    id<GMPolygonDelegate> gon = [polygon polylineWithMapType:self.mapManager.mapViewType];
//    [self.mapManager gm_AddOverlay:gon];
    [self.overlayArray addObject:polygon.polygonDelegate];
    [self.mapManager gm_AddOverlays:self.overlayArray];
}

- (void)mapViewRemoveOverlay
{
    if (self.overlayArray.count == 0) {
        return;
    }
    [self.mapManager gm_RemoveOverlays:self.overlayArray];
}

- (void)mapViewMoveCenterCoor
{
    [self.mapManager gm_SetCenterCoordinate:CLLocationCoordinate2DMake(22.92, 114.46) animated:YES];
}

- (void)mapChangeMapModel
{
    if (self.mapManager.mapType == GMMapTypeStandard) {
        self.mapManager.mapType = GMMapTypeSatellite;
    }
    else
    {
        self.mapManager.mapType = GMMapTypeStandard;
    }
    
}

#pragma mark - GMMapViewDelegate

- (id<GMAnnotationViewDelegate>)gm_mapView:(id<GMMapViewDelegate>)mapView viewForAnnotation:(id<GMAnnotation>)annotation
{
    static NSString * reuseId = @"reuseId";
    GMAnnotationViewManager * annoViewM = [GMAnnotationViewManager shareInstance];
    annoViewM.image = [UIImage imageNamed:@"home_car_green_new"];
    annoViewM.canShowCallout = YES;
    annoViewM.draggable = YES;
    
    // 下面这段代码放在最后执行，待上面相关属性赋值完之后
    id<GMAnnotationViewDelegate> annotationView = [annoViewM mapView:mapView annotation:annotation reuseIdentifier:reuseId];
    return annotationView;
}

- (id<GMOverlayViewDelegate>)gm_mapView:(id<GMMapViewDelegate>)mapView viewForOverlay:(id<GMOverlay>)overlay
{
    GMOverlayViewManager * overlayViewM = [GMOverlayViewManager shareInstance];
    // 根据自己喜好设置不同颜色
    if ([overlay conformsToProtocol:@protocol(GMCircleDelegate)]) {
        overlayViewM.lineWidth    = 5.f;
        overlayViewM.strokeColor  = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.8];
        overlayViewM.fillColor    = [UIColor colorWithRed:1.0 green:0.8 blue:0.0 alpha:0.8];
        overlayViewM.lineDash     = YES;
    }
    else if ([overlay conformsToProtocol:@protocol(GMPolylineDelegate)]) {
        overlayViewM.lineWidth    = 7.f;
        overlayViewM.strokeColor  = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:0.8];
        overlayViewM.fillColor    = [UIColor colorWithRed:1.0 green:0.8 blue:0.0 alpha:0.8];
        overlayViewM.lineDash     = NO;
    }
    else {/* if ([overlay conformsToProtocol:@protocol(GMPolygonDelegate)]) 不知道为什么只有百度和腾讯地图无缘无故不遵守这个协议，各种调试都试过确实是不遵守，努力寻找答案中。。。*/
        overlayViewM.lineWidth    = 4.f;
        overlayViewM.strokeColor  = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.8];
        overlayViewM.fillColor    = [UIColor colorWithRed:0.77 green:0.88 blue:0.94 alpha:0.8];
        overlayViewM.lineDash     = NO;
    }
    
    // 下面这段代码放在最后执行，待上面相关属性赋值完之后
    id<GMOverlayViewDelegate> overlayView = [overlayViewM mapView:mapView overlay:overlay];
    return overlayView;
}

/*!
 @brief  生成多角星坐标
 @param coordinates 输出的多角星坐标数组指针。内存需在外申请，方法内不释放，多角星坐标结果输出。
 @param pointsCount 输出的多角星坐标数组元素个数。
 @param starCenter  多角星的中心点位置。
 */
- (void)generateStarPoints:(CLLocationCoordinate2D *)coordinates pointsCount:(NSUInteger)pointsCount atCenter:(CLLocationCoordinate2D)starCenter
{
#define STAR_RADIUS 0.05
#define PI 3.1415926
    NSUInteger starRaysCount = pointsCount / 2;
    for (int i =0; i<starRaysCount; i++)
    {
        float angle = 2.f*i/starRaysCount*PI;
        int index = 2 * i;
        coordinates[index].latitude = STAR_RADIUS* sin(angle) + starCenter.latitude;
        coordinates[index].longitude = STAR_RADIUS* cos(angle) + starCenter.longitude;
        
        index++;
        angle = angle + (float)1.f/starRaysCount*PI;
        coordinates[index].latitude = STAR_RADIUS/2.f* sin(angle) + starCenter.latitude;
        coordinates[index].longitude = STAR_RADIUS/2.f* cos(angle) + starCenter.longitude;
    }
    
}

- (CGPoint)randomPoint
{
    CGPoint randomPoint = CGPointZero;
    
    randomPoint.x = arc4random() % (int)(CGRectGetWidth(self.view.bounds));
    randomPoint.y = arc4random() % (int)(CGRectGetHeight(self.view.bounds));
    
    return randomPoint;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
