//
//  SMMapHeader.h
//  SMMapSDK
//
//  Created by zhuch on 2017/9/7.
//  Copyright © 2017年 zhuch. All rights reserved.
//

#ifndef SMMapHeader_h
#define SMMapHeader_h

#if 1
#ifndef __BMK__
#define __BMK__
#import <BaiduMapAPI_Base/BMKBaseComponent.h>//引入base相关所有的头文件
#import <BaiduMapAPI_Map/BMKMapComponent.h>//引入地图功能所有的头文件
#import <BaiduMapAPI_Search/BMKSearchComponent.h>//引入检索功能所有的头文件
#import <BaiduMapAPI_Cloud/BMKCloudSearchComponent.h>//引入云检索功能所有的头文件
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件
//#import <BaiduMapAPI_Radar/BMKRadarComponent.h>//引入周边雷达功能所有的头文件
#import <BaiduMapAPI_Map/BMKMapView.h>//只引入所需的单个头文件
#endif
#endif

#if 1
#ifndef __AMap__
#define __AMap__
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#endif
#endif

#if 1
#ifndef __SMap__
#define __SMap__
#import <MapKit/MapKit.h>
#endif
#endif

#if 1
#ifndef __QMap__
#define __QMap__
#import <QMapKit/QMapKit.h>
#endif
#endif

#import "GM_BMKMapView.h"
#import "GM_MAMapView.h"
#import "GM_QMapView.h"
#import "GM_MKMapView.h"


#endif /* SMMapHeader_h */
